export const environment = {
    production: true,
    API_URL: "https://clavem-api.herokuapp.com",
    GRAPHQL_URI: "https://clavem-api.herokuapp.com/graphql",
    firebase: {
        apiKey: "AIzaSyBsD6dwAxin84wAkY2D8_hwh6IX_cwTPag",
        authDomain: "push-notifs-eb4e2.firebaseapp.com",
        databaseURL: "https://push-notifs-eb4e2.firebaseio.com",
        projectId: "push-notifs-eb4e2",
        storageBucket: "push-notifs-eb4e2.appspot.com",
        messagingSenderId: "946613136320",
        appId: "1:946613136320:web:11e148a103d65f88403625",
        measurementId: "G-8YLYZFXZEM"
    }
};
