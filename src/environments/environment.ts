// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    API_URL: "https://clavem-api.herokuapp.com",
    GRAPHQL_URI: "http://localhost:3000/graphql",//"https://clavem-api.herokuapp.com/graphql",
    firebase: {
        apiKey: "AIzaSyBsD6dwAxin84wAkY2D8_hwh6IX_cwTPag",
        authDomain: "push-notifs-eb4e2.firebaseapp.com",
        databaseURL: "https://push-notifs-eb4e2.firebaseio.com",
        projectId: "push-notifs-eb4e2",
        storageBucket: "push-notifs-eb4e2.appspot.com",
        messagingSenderId: "946613136320",
        appId: "1:946613136320:web:11e148a103d65f88403625",
        measurementId: "G-8YLYZFXZEM"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
