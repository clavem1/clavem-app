import { AuthService } from './../auth/service/auth.service';
import { UserRole } from './../users/enums/user-role';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-particpant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.scss']
})
export class ParticipantComponent implements OnInit {

  userRole: UserRole;

  constructor(public authServie: AuthService) { }

  ngOnInit(): void {
    this.getUserRole();
  }

  getUserRole() {
    this.userRole = this.authServie.getCurrentRole();
  }

}
