import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParticpantRoutingModule } from './participant-routing.module';
import { ParticipantComponent } from './participant.component';
import { InvitationFormComponent } from './invitation-form/invitation-form.component';
import { ReservationComponent } from './reservation/reservation.component';
import { InvitationComponent } from './invitation/invitation.component';
import { TemplateModule } from '../shared/components/template/template.module';


@NgModule({
  declarations: [ParticipantComponent, InvitationFormComponent, ReservationComponent, InvitationComponent],
  imports: [
    CommonModule,
    ParticpantRoutingModule,
    TemplateModule
  ]
})
export class ParticipantModule { }
