import { UserRole } from './../../users/enums/user-role';
import { AuthService } from './../../auth/service/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.scss']
})
export class InvitationComponent implements OnInit {

  userRole: UserRole;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.getUserRole();
  }

  getUserRole() {
    this.userRole = this.authService.getCurrentRole();
  }

}
