import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FetchEventReservationsGQL } from 'src/generated/graphql';
import { AuthService } from '../../auth/service/auth.service';
import { UserRole } from '../../users/enums/user-role';

@Component({
    selector: 'app-reservation',
    templateUrl: './reservation.component.html',
    styleUrls: ['./reservation.component.scss']
})
export class ReservationComponent implements OnInit {
    reservations = [];
    userRole: UserRole;
    eventId: string;
    constructor(
        private authService: AuthService,
        private readonly fetchEventReservationsGQL: FetchEventReservationsGQL,
        private readonly activatedRoute: ActivatedRoute,
    ) { }

    ngOnInit(): void {
        this.eventId = this.activatedRoute.snapshot.paramMap.get('id');
        this.fetchEventReservationsGQL.fetch({eventId: this.eventId}).subscribe(
            (result) => {
                this.reservations = result.data.fetchEventReservations;
            }
        )
        this.getUserRole();
    }
    getUserRole() {
        this.userRole = this.authService.getCurrentRole();
    }
}
