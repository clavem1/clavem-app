import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParticipantComponent } from './participant.component';
import { InvitationComponent } from './invitation/invitation.component';
import { ReservationComponent } from './reservation/reservation.component';
import { InvitationFormComponent } from './invitation-form/invitation-form.component';

const routes: Routes = [
  { path: '', component: ParticipantComponent },
  { path: 'invitation', component: InvitationComponent },
  { path: 'invitation/create', component: InvitationFormComponent },
  { path: 'reservation', component: ReservationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParticpantRoutingModule { }
