import { Component, OnInit, ViewChild } from '@angular/core';
import { CreateCategoryGQL, CategoryEntity } from 'src/generated/graphql';
import { CategoryService } from '../services/category.service';

@Component({
    selector: 'app-new-category',
    templateUrl: './new-category.component.html',
    styleUrls: ['./new-category.component.scss']
})
export class NewCategoryComponent implements OnInit {
    name: string;
    createCategoryResult: { status: string; message: string; };
    constructor(
        private readonly createCategoryGQL: CreateCategoryGQL,
        private readonly categoryService: CategoryService,
    ) { }

    ngOnInit(): void {
    }

    createCategory() {
        if (this.name && this.name.trim() != "") {
            this.createCategoryGQL.mutate({ categoryInput: { name: this.name } }).subscribe(
                (result) => {
                    if (result.errors) {
                        this.createCategoryResult = { status: 'danger', message: 'Erreur lors de la création de la Catégorie!' };
                    }
                    if (result.data) {
                        let createdCategory = result.data.createCategory;
                        this.categoryService.emitNewCategory(createdCategory as CategoryEntity);
                        this.createCategoryResult = { status: 'success', message: 'Catégorie créée avec succés!' };
                        this.name = "";
                    }
                }
            )
        }
    }
}
