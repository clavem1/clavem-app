import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category.component';
import { TemplateModule } from '../shared/components/template/template.module';
import { NewCategoryComponent } from './new-category/new-category.component';
import { FormsModule } from '@angular/forms';
import { CategoryService } from './services/category.service';

@NgModule({
    declarations: [CategoryComponent, NewCategoryComponent],
    imports: [
        CommonModule,
        CategoryRoutingModule,
        TemplateModule,
        FormsModule,
    ],
    providers: [
        CategoryService,
    ]
})
export class CategoryModule { }
