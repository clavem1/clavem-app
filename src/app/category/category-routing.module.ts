import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './category.component';
import { NewCategoryComponent } from './new-category/new-category.component';
import { EventComponent } from '../event/event.component';


const routes: Routes = [
    { path: '', component: CategoryComponent },
    { path: ':id', component: EventComponent },
    { path: 'new', component: NewCategoryComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CategoryRoutingModule { }
