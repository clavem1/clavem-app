import { Injectable } from "@angular/core";
import { Subject } from 'rxjs';
import { CategoryEntity } from 'src/generated/graphql';

@Injectable()
export class CategoryService {
    public newCategorySubject: Subject<CategoryEntity> = new Subject<CategoryEntity>();

    emitNewCategory(category: CategoryEntity) {
        this.newCategorySubject.next(category);
    }
}
