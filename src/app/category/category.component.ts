import { UserRole } from './../users/enums/user-role';
import { AuthService } from './../auth/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FetchCategoriesGQL, CategoriesEntity, CategoryEntity } from 'src/generated/graphql';
import { CategoryService } from './services/category.service';
import { NgForm } from '@angular/forms';
import { logWarnings } from 'protractor/built/driverProviders';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
    categories;
    categoriesList;
    userRole: UserRole;
    constructor(
        public authService: AuthService,
        private readonly fetchCategoriesGQL: FetchCategoriesGQL,
        private readonly categoryService: CategoryService,
    ) { }

    ngOnInit(): void {
        this.categoryService.newCategorySubject.subscribe(
            (category: CategoryEntity) => {
                this.categories.records.push(category);
            }
        )
        this.fetchCategoriesGQL.fetch({}).subscribe(
            (result) => {
                this.categories = result.data.fetchCategories;
                this.categoriesList = this.categories.records;
            }
        );
        this.getUserRole();
    }

    getUserRole() {
        this.userRole = this.authService.getCurrentRole();
    }

    onSearch(value: NgForm) {
        this.categoriesList = this.categories.records;
        if (value['rechercherPar'] == "") {
            this.categoriesList = this.categories.records;
        }
        this.categoriesList = this.categoriesList.filter(res => {
            if (String(res.paidEntrance).match(value['rechercherPar']) !== null) {
                return true;
            }
            if (String(res.freeEntrance).match(value['rechercherPar']) !== null) {
                return true;
            }
            if (String(res.pending).match(value['rechercherPar']) !== null) {
                return true;
            }
            if (String(res.refused).match(value['rechercherPar']) !== null) {
                return true;
            }
            if (String(res.validated).match(value['rechercherPar']) !== null) {
                return true;
            }
            if (String(res.archived).match(value['rechercherPar']) !== null) {
                return true;
            }
            if (res.name.toLowerCase().match(value['rechercherPar'].toLowerCase()) !== null) {
                return true;
            }
        })
    }
}
