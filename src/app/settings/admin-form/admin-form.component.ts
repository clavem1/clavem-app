import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AnyObject } from 'src/app/shared/typings/typescript';
import { AdminService } from '../services/admin.service';

@Component({
    selector: 'app-admin-form',
    templateUrl: './admin-form.component.html',
    styleUrls: ['./admin-form.component.scss']
})
export class AdminFormComponent implements OnInit {
    form: AnyObject = { internalRole: "STANDARD" };
    avatar;
    createResult: { message: string; status: string };
    constructor(
        private readonly adminService: AdminService,
    ) { }

    ngOnInit(): void {
    }

    onAvatarChange(event) {
        this.avatar = event.target.files[0];
    }


    submit() {
        const formData = new FormData();
        formData.append('avatar', this.avatar);
        const fields = Object.keys(this.form);
        fields.map((field: string) => {
            formData.append(field, this.form[field]);
        });
        this.adminService.createAdmin(formData).subscribe(
            (result) => {
                this.createResult = { message: "Utilisateur créé avec succés!", status: "success" };
                this.form = {};
            },
            (error) => {
                this.createResult = { message: "Erreur lors de la création de l'utilisateur!", status: "danger" };
            }
        )
    }
}
