import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class AdminService {
    constructor(private readonly httpClient: HttpClient) { }

    createAdmin(userDto) {
        return this.httpClient.post(`${environment.API_URL}/user/create-admin`, userDto);
    }
}
