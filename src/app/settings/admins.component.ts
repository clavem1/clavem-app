import { Component, OnInit } from '@angular/core';
import result, { FetchAdminsGQL } from 'src/generated/graphql';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-admins',
    templateUrl: './admins.component.html',
    styleUrls: ['./admins.component.scss']
})
export class AdminsComponent implements OnInit {
    admins;
    baseAvatarAPI = `${environment.API_URL}/multimedia/images/avatar`;
    constructor(
        private readonly fetchAdminsGQL: FetchAdminsGQL,
    ) { }

    ngOnInit(): void {
        this.fetchAdminsGQL.fetch().subscribe(
            (result) => {
                this.admins = result.data.fetchAdmins.records;
            }
        )
    }
    generateUrl(url) {
        return this.baseAvatarAPI + '/' + url;
    }
}
