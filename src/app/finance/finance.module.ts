import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceRoutingModule } from './finance-routing.module';
import { InvoicesComponent } from './invoices/invoices.component';
import { ReclamationsComponent } from './reclamations/reclamations.component';
import { FinanceDashboardComponent } from './finance-dashboard/finance-dashboard.component';
import { TemplateModule } from '../shared/components/template/template.module';
import { RevenueComponent } from './finance-dashboard/revenue/revenue.component';
import { CommissionComponent } from './finance-dashboard/commission/commission.component';
import { TransactionComponent } from './finance-dashboard/transaction/transaction.component';
import {InvoiceUsersComponent} from './invoices/invoice-users/invoice-users.component';
import {InvoiceOrganizersComponent} from './invoices/invoice-organizers/invoice-organizers.component';
import { RecipesComponent } from './recipes/recipes.component';


@NgModule({
  declarations: [InvoicesComponent,
      ReclamationsComponent,
      FinanceDashboardComponent,
      RevenueComponent,
      CommissionComponent,
      TransactionComponent,
      InvoiceUsersComponent,
      InvoiceOrganizersComponent,
      RecipesComponent
  ],
    imports: [
        CommonModule,
        FinanceRoutingModule,
        TemplateModule,
    ]
})
export class FinanceModule { }
