import { FinanceDashboardComponent } from './finance-dashboard/finance-dashboard.component';
import { ReclamationsComponent } from './reclamations/reclamations.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommissionComponent } from './finance-dashboard/commission/commission.component';
import { RevenueComponent } from './finance-dashboard/revenue/revenue.component';
import { TransactionComponent } from './finance-dashboard/transaction/transaction.component';
import {RecipesComponent} from './recipes/recipes.component';


const routes: Routes = [
    { path: 'dashboard', component: FinanceDashboardComponent },
    { path: 'dashboard/commission', component: CommissionComponent },
    { path: 'dashboard/revenue', component: RevenueComponent },
    { path: 'dashboard/transaction', component: TransactionComponent },
    { path: 'recipes', component: RecipesComponent },
    { path: 'invoices', component: InvoicesComponent },
    { path: 'reclamations', component: ReclamationsComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FinanceRoutingModule { }
