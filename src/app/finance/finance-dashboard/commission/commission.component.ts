import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/service/auth.service';
import { UserRole } from 'src/app/users/enums/user-role';

@Component({
  selector: 'app-commission',
  templateUrl: './commission.component.html',
  styleUrls: ['./commission.component.scss']
})
export class CommissionComponent implements OnInit {

  userRole: UserRole;

  constructor(private authService: AuthService) {
    this.getUserRole();
  }

  ngOnInit(): void {
  }

  getUserRole() {
    this.userRole = this.authService.getCurrentRole();
  }

}
