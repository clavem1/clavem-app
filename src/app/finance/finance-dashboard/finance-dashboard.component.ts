import { AuthService } from './../../auth/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { UserRole } from 'src/app/users/enums/user-role';

@Component({
  selector: 'app-finance-dashboard',
  templateUrl: './finance-dashboard.component.html',
  styleUrls: ['./finance-dashboard.component.scss']
})
export class FinanceDashboardComponent implements OnInit {

  userRole: UserRole;
  commission = true;
  revenue = false;
  transaction = false;

  constructor(private authService: AuthService) {
    this.getUserRole();
  }

  ngOnInit(): void {
  }

  getUserRole() {
    this.userRole = this.authService.getCurrentRole();
  }
  toggleCommission() {
    this.commission = true;
    this.revenue = this.transaction = false;
  }
  toggleRevene() {
    this.revenue = true;
    this.commission = this.transaction = false;
  }
  toggleTransaction() {
    this.transaction = true;
    this.commission = this.revenue = false;
  }
}
