import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/service/auth.service';
import { UserRole } from 'src/app/users/enums/user-role';

@Component({
  selector: 'app-revenue',
  templateUrl: './revenue.component.html',
  styleUrls: ['./revenue.component.scss']
})
export class RevenueComponent implements OnInit {

  userRole: UserRole;

  constructor(private authService: AuthService) {
    this.getUserRole();
  }

  ngOnInit(): void {
  }

  getUserRole() {
    this.userRole = this.authService.getCurrentRole();
  }

}
