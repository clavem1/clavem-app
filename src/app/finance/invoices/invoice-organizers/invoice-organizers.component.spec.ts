import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceOrganizersComponent } from './invoice-organizers.component';

describe('InvoiceOrganizersComponent', () => {
  let component: InvoiceOrganizersComponent;
  let fixture: ComponentFixture<InvoiceOrganizersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceOrganizersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceOrganizersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
