import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../auth/service/auth.service';
import {UserRole} from '../../../users/enums/user-role';

@Component({
    selector: 'app-invoice-organizers',
    templateUrl: './invoice-organizers.component.html',
    styleUrls: ['./invoice-organizers.component.scss']
})
export class InvoiceOrganizersComponent implements OnInit {

    userRole: UserRole;
    constructor(private authService: AuthService) { }

    ngOnInit(): void {
        this.getUserRole();
    }

    getUserRole() {
        this.userRole = this.authService.getCurrentRole();
    }
}
