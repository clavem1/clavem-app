import { Component, OnInit } from '@angular/core';
import {UserRole} from '../../../users/enums/user-role';
import {AuthService} from '../../../auth/service/auth.service';

@Component({
    selector: 'app-invoice-users',
    templateUrl: './invoice-users.component.html',
    styleUrls: ['./invoice-users.component.scss']
})
export class InvoiceUsersComponent implements OnInit {

    userRole: UserRole;
    constructor(private authService: AuthService) { }

    ngOnInit(): void {
        this.getUserRole();
    }
    getUserRole() {
        this.userRole = this.authService.getCurrentRole();
    }

}
