import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../auth/service/auth.service';
import {UserRole} from "../../users/enums/user-role";

@Component({
    selector: 'app-invoices',
    templateUrl: './invoices.component.html',
    styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {

    users = true;
    organizer = false;
    userRole: UserRole;
    constructor(private authService: AuthService) { }

    ngOnInit(): void {
        this.getUserRole();
    }

    getUserRole() {
        this.userRole = this.authService.getCurrentRole();
    }
    toggleUsers() {
        this.users = true;
        this.organizer = false;
    }

    toggelOrganizer() {
        this.organizer = true;
        this.users = false;
    }
}
