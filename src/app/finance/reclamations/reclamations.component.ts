import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/service/auth.service'
import { UserRole } from '../../users/enums/user-role'

@Component({
  selector: 'app-reclamations',
  templateUrl: './reclamations.component.html',
  styleUrls: ['./reclamations.component.scss']
})
export class ReclamationsComponent implements OnInit {

  userRole: UserRole;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.getUserRole();
  }

  getUserRole() {
    this.userRole = this.authService.getCurrentRole();
  }
}
