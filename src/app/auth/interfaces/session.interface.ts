import { IUser } from '../../users/interfaces/user.interface';

export interface ISession {
  token: string;
  user: IUser;
}
