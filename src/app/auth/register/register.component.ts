import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder } from '@angular/forms';
import { phoneNumberValidator, confirmPasswordValidator } from 'src/app/shared/forms/validators/validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  errorMessage: string;
  constructor(private readonly authService: AuthService, private readonly router: Router, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group(
      {
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.required, Validators.minLength(3)]),
        confirmPassword: new FormControl('', [Validators.required]),
        phoneNumber: new FormControl('', [Validators.required, phoneNumberValidator])
      },
      { validator: confirmPasswordValidator }
    );
  }

  get lastName() {
    return this.registerForm.get('lastName');
  }

  get firstName() {
    return this.registerForm.get('firstName');
  }

  get email() {
    return this.registerForm.get('email');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

  get phoneNumber() {
    return this.registerForm.get('phoneNumber');
  }

  signup() {
    this.authService.signup(this.registerForm.value).subscribe(
      (response: any) => {
        this.authService.registerCurrentSession(response);
        this.authService.registerToken(response.token);
        this.router.navigate(['/dashboard']);
      },
      (httpError: HttpErrorResponse) => {
        this.errorMessage = httpError.error.message;
      }
    );
  }

  passwordsAreEqual(): boolean {
    return this.registerForm.value.password.trim() === this.registerForm.value.confirmPassword.trim() && this.registerForm.value.password.trim() != "";
  }

}
