import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errorMessage: string;
  registerForm: FormGroup;
  constructor(private readonly authService: AuthService, private readonly router: Router) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup(
      {
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', Validators.required),
      }
    );
  }

  signin() {
    this.authService.signin(this.registerForm.value).subscribe(
      (response: any) => {
        this.authService.registerCurrentSession(response);
        this.authService.registerToken(response.token);
        this.router.navigate(['/dashboard']);
      },
      (httpError: HttpErrorResponse) => {
        this.errorMessage = httpError.error.message;
      }
    );
  }
}
