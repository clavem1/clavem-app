import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TicketingRoutingModule } from './ticketing-routing.module';
import { TicketingComponent } from './ticketing.component';
import { TicketingFormComponent } from './ticketing-form/ticketing-form.component';
import { TicketingListComponent } from './ticketing-list/ticketing-list.component';
import { TemplateModule } from '../shared/components/template/template.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
    declarations: [TicketingComponent, TicketingFormComponent, TicketingListComponent],
    imports: [
        CommonModule,
        TicketingRoutingModule,
        TemplateModule,
        FormsModule,
        Ng2SearchPipeModule,
        ReactiveFormsModule,
    ]
})
export class TicketingModule { }
