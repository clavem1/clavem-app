import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CreateTicketGQL } from 'src/generated/graphql';

@Component({
    selector: 'app-ticketing-form',
    templateUrl: './ticketing-form.component.html',
    styleUrls: ['./ticketing-form.component.scss']
})
export class TicketingFormComponent implements OnInit {
    form: FormGroup;
    useGenderCriteria: boolean = false;
    useAgeCriteria: boolean = false;
    age: number;
    resultMessage: { status: string; message: string; };
    @Input() eventId;
    @Output() formEmitter = new EventEmitter();
    constructor(private readonly createTicketGQL: CreateTicketGQL) { }

    ngOnInit(): void {
        this.initForm();
    }

    initForm() {
        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            categoryCriteria: new FormControl('', Validators.required),
            quantity: new FormControl('', Validators.required),
            price: new FormControl('', Validators.required),
        });
    }

    get price() { return this.form.controls.price.value };

    submit() {
        this.createTicketGQL.mutate({ eventId: this.eventId, ticketDto: this.form.value }).subscribe(
            (result) => {
                if (!result.errors) {
                    this.resultMessage = { status: "success", message: "Ticket créé avec succés!" };
                    this.formEmitter.emit(result.data.createTicket);
                } else {
                    this.resultMessage = { status: "danger", message: "Erreur lors de la création du ticket!" };
                }
            },
            (error) => {
                this.resultMessage = { status: "danger", message: "Erreur lors de la création du ticket!" };
            }
        )
    }

}
