import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/service/auth.service';
import { UserRole } from "../../users/enums/user-role";
import { ActivatedRoute } from '@angular/router';
import { AnyObject } from 'src/app/shared/typings/typescript';
import { FetchEventGQL, RemoveTicketGQL } from 'src/generated/graphql';

@Component({
    selector: 'app-ticketing-list',
    templateUrl: './ticketing-list.component.html',
    styleUrls: ['./ticketing-list.component.scss']
})
export class TicketingListComponent implements OnInit {
    event: AnyObject;
    eventId: string;
    search: string = "";
    userRole: UserRole;
    constructor(
        private authServie: AuthService,
        private readonly activatedRoute: ActivatedRoute,
        private readonly fetchEventGQL: FetchEventGQL,
        private readonly removeTicketGQL: RemoveTicketGQL,
    ) { }

    ngOnInit(): void {
        this.getUserRole();
        this.eventId = this.activatedRoute.snapshot.paramMap.get('id');
        this.fetchEventGQL.fetch({ eventId: this.eventId }).subscribe(
            (result) => {
                this.event = result.data.fetchEvent;
            }
        );
    }

    getUserRole() {
        this.userRole = this.authServie.getCurrentRole();
    }

    eventChanged(event) {
        this.event = event;
    }

    removeTicket(ticketId) {
        this.removeTicketGQL.mutate({ eventId: this.eventId, ticketId: ticketId }).subscribe(
            (result) => {
                if (!result.errors) {
                    this.event = result.data.removeTicket;
                } else {
                    alert('Une erreur lors de la suppression du ticket!');
                }
            },
            (error) => {
                alert('Une erreur lors de la suppression du ticket!');
            }
        )
    }
}
