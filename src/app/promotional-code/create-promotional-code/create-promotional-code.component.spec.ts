import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePromotionalCodeComponent } from './create-promotional-code.component';

describe('CreatePromotionalCodeComponent', () => {
  let component: CreatePromotionalCodeComponent;
  let fixture: ComponentFixture<CreatePromotionalCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePromotionalCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePromotionalCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
