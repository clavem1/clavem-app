import { Component, OnInit } from '@angular/core';
import { FetchPromotionalCodesGQL } from 'src/generated/graphql';

@Component({
    selector: 'app-promotional-code',
    templateUrl: './promotional-code.component.html',
    styleUrls: ['./promotional-code.component.scss']
})
export class PromotionalCodeComponent implements OnInit {
    promotionalCodes;
    constructor(
        private readonly fetchPromotionalCodesGQL: FetchPromotionalCodesGQL
    ) { }

    ngOnInit(): void {
        this.fetchPromotionalCodesGQL.fetch().subscribe(
            (result) => {
                this.promotionalCodes = result.data.fetchPromotionalCodes.records;
            }
        )
    }

}
