import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromotionalCodeComponent } from './promotional-code.component';
import { CreatePromotionalCodeComponent } from './create-promotional-code/create-promotional-code.component';


const routes: Routes = [
  { path: '', component: PromotionalCodeComponent },
  { path: 'create', component: CreatePromotionalCodeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromotionalCodeRoutingModule { }
