import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotionalCodeFormComponent } from './promotional-code-form.component';

describe('PromotionalCodeFormComponent', () => {
  let component: PromotionalCodeFormComponent;
  let fixture: ComponentFixture<PromotionalCodeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotionalCodeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotionalCodeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
