import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { FetchEventsGQL, PromotionalCodeImpact, CreatePromotionalCodeGQL } from 'src/generated/graphql';

@Component({
    selector: 'app-promotional-code-form',
    templateUrl: './promotional-code-form.component.html',
    styleUrls: ['./promotional-code-form.component.scss']
})
export class PromotionalCodeFormComponent implements OnInit {
    form: FormGroup;
    events;
    selectedEvent;
    promotionalCodeImpact = PromotionalCodeImpact;
    submitResult;
    constructor(
        private readonly formBuilder: FormBuilder,
        private readonly fetchEventsGQL: FetchEventsGQL,
        private readonly createPromotionalCodeGQL: CreatePromotionalCodeGQL,
    ) { }

    ngOnInit(): void {
        this.fetchEventsGQL.fetch().subscribe(
            (result) => {
                this.events = result.data.fetchEvents.records;
                this.selectedEvent = this.events.length ? this.events[0] : null;
                const eventId = this.form.get('eventId');
                eventId.setValue(this.selectedEvent._id)
            }
        )
        this.initForm();
        this.form.valueChanges.subscribe(
            (value) => {
                // console.log(value);
            }
        )
    }

    initForm() {
        this.form = this.formBuilder.group({
            eventId: ['', Validators.required],
            privateName: ['', Validators.required],
            publicName: ['', Validators.required],
            tickets: new FormArray([]),
            usableNumberOfTimes: [0, [Validators.required, Validators.min(1)]],
            numberOfGeneratedCodes: [0, [Validators.required, Validators.min(1)]],
            impact: ['', Validators.required],
            reductionEffect: this.formBuilder.group({
                inPercentage: [true, [Validators.required]],
                reduction: [0, [Validators.required, Validators.min(0)]],
                reductionInPercentage: [0, [Validators.required, Validators.min(0)]],
                reductionInUS: [0, [Validators.required, Validators.min(0)]]
            }),
            startDate: [new Date(), Validators.required],
            endDate: [new Date(), Validators.required],
        });
    }

    get reductionEffect() { return this.form.controls.reductionEffect as FormGroup }

    onChangeEvent($event) {
        const tickets: FormArray = this.form.get('tickets') as FormArray;
        tickets.clear();
        this.selectedEvent = this.events.filter((event) => {
            return event._id === $event.target.value;
        })[0];
    }

    onChangeTickets(event) {
        const tickets: FormArray = this.form.get('tickets') as FormArray;
        const ticket = event.target.value;
        if (event.target.checked) {
            tickets.push(new FormControl(ticket));
        }
        else {
            let i: number = 0;

            tickets.controls.forEach((ctrl: FormControl) => {
                if (ctrl.value == ticket) {
                    tickets.removeAt(i);
                    return;
                }
                i++;
            });
        }
    }

    onChangeReductionMode() {
        const inPercentage = this.reductionEffect.value.inPercentage;
        if (inPercentage) {
            this.reductionEffect.get('reduction').setValue(this.reductionEffect.controls.reductionInPercentage.value);
        } else {
            this.reductionEffect.get('reduction').setValue(this.reductionEffect.controls.reductionInUS.value);
        }
    }

    submitForm() {
        delete this.form.value.reductionEffect.reductionInPercentage;
        delete this.form.value.reductionEffect.reductionInUS;
        console.log(this.form.value);
        this.createPromotionalCodeGQL.mutate({ promotionalCodeDto: this.form.value }).subscribe(
            (result) => {
                if (result.errors) {
                    this.submitResult = { status: 'danger', message: 'Erreur lors de la création!' };
                } else {
                    this.submitResult = { status: 'success', message: 'Votre code promotionnel est créé avec succés!' };
                }
            },
            (error) => {
                this.submitResult = { status: 'danger', message: 'Erreur lors de la création!' };
            }
        )
    }
}
