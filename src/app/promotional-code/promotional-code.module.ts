import { TemplateModule } from './../shared/components/template/template.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromotionalCodeRoutingModule } from './promotional-code-routing.module';
import { PromotionalCodeComponent } from './promotional-code.component';
import { PromotionalCodeFormComponent } from './promotional-code-form/promotional-code-form.component';
import { CreatePromotionalCodeComponent } from './create-promotional-code/create-promotional-code.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
    declarations: [PromotionalCodeComponent, PromotionalCodeFormComponent, CreatePromotionalCodeComponent],
    imports: [
        CommonModule,
        TemplateModule,
        PromotionalCodeRoutingModule,
        ReactiveFormsModule,
        FormsModule,
    ]
})
export class PromotionalCodeModule { }
