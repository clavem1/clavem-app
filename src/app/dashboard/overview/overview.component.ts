import { UserRole } from './../../users/enums/user-role';
import { AuthService } from './../../auth/service/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  userRole: UserRole;

  constructor(private authService: AuthService) {
    this.getUserRole();
  }

  ngOnInit(): void {
  }

 getUserRole() {
    this.userRole = this.authService.getCurrentRole();
  }


}
