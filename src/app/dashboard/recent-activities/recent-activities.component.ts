import { Component, OnInit } from '@angular/core';
import { FetchRecentEventsGQL } from 'src/generated/graphql';

@Component({
    selector: 'app-recent-activities',
    templateUrl: './recent-activities.component.html',
    styleUrls: ['./recent-activities.component.scss']
})
export class RecentActivitiesComponent implements OnInit {
    events;
    constructor(
        private readonly fetchRecentEventsGQL: FetchRecentEventsGQL,
    ) { }

    ngOnInit(): void {
        this.fetchRecentEventsGQL.fetch().subscribe(
            (result) => {
                this.events = result.data.fetchRecentEvents.records;
            }
        )
    }

}
