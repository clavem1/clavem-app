import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { OverviewComponent } from './overview/overview.component';
import { RecentActivitiesComponent } from './recent-activities/recent-activities.component';
import { TemplateModule } from '../shared/components/template/template.module';

@NgModule({
  declarations: [DashboardComponent, OverviewComponent, RecentActivitiesComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    TemplateModule,
  ],
  providers: []
})
export class DashboardModule { }
