import { MarktingComponent } from './markting.component';
import { TemplateModule } from './../shared/components/template/template.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MarktingRoutingModule } from './markting-routing.module';
import { IntraPlatformComponent } from './intra-platform/intra-platform.component';
import { EvaluationComponent } from './evaluation/evaluation.component';
import { PromoteEventComponent } from './promote-event/promote-event.component';
import { ShareComponent } from './share/share.component';
import { AdvertisingWallComponent } from './intra-platform/advertising-wall/advertising-wall.component';
import { CampaignManagementComponent } from './intra-platform/campaign-management/campaign-management.component';
import { NewsFeedManagementComponent } from './intra-platform/news-feed-management/news-feed-management.component';
import { AddPublicationComponent } from './intra-platform/advertising-wall/add-publication/add-publication.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddCampaignComponent } from './intra-platform/campaign-management/add-campaign/add-campaign.component';
import { AddPromoteEventComponent } from './promote-event/add-promote-event/add-promote-event.component';
import { EmplacementUpComponent } from './intra-platform/news-feed-management/emplacement-up/emplacement-up.component';
import { OtherComponent } from './intra-platform/news-feed-management/other/other.component';
import { AddImageEmplacementUpComponent } from './intra-platform/news-feed-management/emplacement-up/add-image-emplacement-up/add-image-emplacement-up.component';
import { AddImageOtherComponent } from './intra-platform/news-feed-management/other/add-image-other/add-image-other.component';
import { PublicationService } from './intra-platform/advertising-wall/add-publication/publication.service';

@NgModule({
  declarations: [
    MarktingComponent,
    IntraPlatformComponent,
    EvaluationComponent,
    PromoteEventComponent,
    ShareComponent,
    AdvertisingWallComponent,
    CampaignManagementComponent,
    NewsFeedManagementComponent,
    AddPublicationComponent,
    AddPromoteEventComponent,
    AddCampaignComponent,
    EmplacementUpComponent,
    OtherComponent,
    AddImageEmplacementUpComponent,
    AddImageOtherComponent],
  imports: [
    CommonModule,
    MarktingRoutingModule,
    TemplateModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
      PublicationService,
  ]
})
export class MarktingModule {

}
