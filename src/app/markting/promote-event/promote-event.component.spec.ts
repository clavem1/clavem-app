import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoteEventComponent } from './promote-event.component';

describe('PromoteEventComponent', () => {
  let component: PromoteEventComponent;
  let fixture: ComponentFixture<PromoteEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoteEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoteEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
