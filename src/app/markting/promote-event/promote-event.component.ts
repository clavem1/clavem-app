import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-promote-event',
    templateUrl: './promote-event.component.html',
    styleUrls: ['./promote-event.component.scss']
})
export class PromoteEventComponent implements OnInit {

    image: boolean;
    constructor() { }

    ngOnInit(): void {
    }

    onAddImage() {
        this.image = true;
    }

    onRemoveImage() {
        this.image = false;
    }
}
