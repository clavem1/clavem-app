import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPromoteEventComponent } from './add-promote-event.component';

describe('AddPromoteEventComponent', () => {
  let component: AddPromoteEventComponent;
  let fixture: ComponentFixture<AddPromoteEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPromoteEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPromoteEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
