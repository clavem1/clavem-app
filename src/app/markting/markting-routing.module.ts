import { MarktingComponent } from './markting.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdvertisingWallComponent} from './intra-platform/advertising-wall/advertising-wall.component';
import {CampaignManagementComponent} from './intra-platform/campaign-management/campaign-management.component';
import {NewsFeedManagementComponent} from './intra-platform/news-feed-management/news-feed-management.component';
import {PromoteEventComponent} from './promote-event/promote-event.component';


const routes: Routes = [
    { path: 'advertising-wall', component: AdvertisingWallComponent },
    { path: 'campaign-management', component: CampaignManagementComponent },
    { path: 'news-feed', component: NewsFeedManagementComponent },
    { path: 'promote-event', component: PromoteEventComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MarktingRoutingModule { }
