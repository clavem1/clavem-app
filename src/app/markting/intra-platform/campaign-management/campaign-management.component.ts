import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-campaign-management',
  templateUrl: './campaign-management.component.html',
  styleUrls: ['./campaign-management.component.scss']
})
export class CampaignManagementComponent implements OnInit {

    image: boolean;
    constructor() { }

    ngOnInit(): void {
    }

    onAddImage() {
        this.image = true;
    }

    onRemoveImage() {
        this.image = false;
    }
}
