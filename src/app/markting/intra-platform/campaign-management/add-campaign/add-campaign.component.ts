import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-add-campaign',
    templateUrl: './add-campaign.component.html',
    styleUrls: ['./add-campaign.component.scss']
})
export class AddCampaignComponent implements OnInit {

    @Input() image: boolean;
    dateNumber = 1;
    etape1 = false;
    etape2 = true;
    etape3 = true;
    etape4 = true;
    etape5 = true;
    etape6 = true;
    etape7 = true;
    visa = true;
    orangeMoney = false;
    expresso = false;
    freeMoney = false;

    constructor() { }

    ngOnInit(): void {
    }

    visaSelected() {
        this.visa = true;
        this.orangeMoney = this.expresso = this.freeMoney = false;
    }

    orangeSelected() {
        this.orangeMoney = true;
        this.visa = this.expresso = this.freeMoney = false;
    }

    freeSelected() {
        this.freeMoney = true;
        this.orangeMoney = this.visa = this.expresso = false;
    }

    expressoSelected() {
        this.expresso = true;
        this.orangeMoney = this.visa = this.freeMoney = false;
    }

    onSubmit(form: NgForm) {
        console.log(form);

    }

    previous() {
        if (this.etape6 === false) {
            this.toggleEtape5();
        } else if (this.etape5 == false) {
            this.toggleEtape4();
        } else if (this.etape4 === false) {
            this.toggleEtape3();
        } else if (this.etape3 === false) {
            this.toggleEtape2();
        } else if (this.etape2 === false) {
            this.toggleEtape1();
        }
    }

    next() {
        if (this.etape1 === false) {
            this.toggleEtape2();
        } else if (this.etape2 === false) {
            this.toggleEtape3();
        } else if (this.etape3 === false) {
            this.toggleEtape4();
        } else if (this.etape4 === false) {
            this.toggleEtape5();
        } else if (this.etape5 === false) {
            this.toggleEtape6();
        } else {
            this.toggleEtape7();
        }

    }

    toggleEtape1() {
        this.etape1 = false;
        this.etape2 = this.etape3 = this.etape4 = this.etape5 = this.etape6 = this.etape7 = true;
    }
    toggleEtape2() {
        this.etape2 = false;
        this.etape1 = this.etape3 = this.etape4 = this.etape5 = this.etape6 = this.etape7 = true;
    }
    toggleEtape3() {
        this.etape3 = false;
        this.etape2 = this.etape1 = this.etape4 = this.etape5 = this.etape6 = this.etape7 = true;
    }
    toggleEtape4() {
        this.etape4 = false;
        this.etape2 = this.etape3 = this.etape1 = this.etape5 = this.etape6 = this.etape7 = true;
    }
    toggleEtape5() {
        this.etape5 = false;
        this.etape2 = this.etape3 = this.etape4 = this.etape1 = this.etape6 = this.etape7 = true;
    }
    toggleEtape6() {
        this.etape6 = false;
        this.etape2 = this.etape3 = this.etape4 = this.etape5 = this.etape1 = this.etape7 = true;
    }
    toggleEtape7() {
        this.etape7 = false;
        this.etape2 = this.etape3 = this.etape4 = this.etape5 = this.etape6 = this.etape1 = true;
    }
}
