import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-intra-platform',
    templateUrl: './intra-platform.component.html',
    styleUrls: ['./intra-platform.component.scss']
})
export class IntraPlatformComponent implements OnInit {

    adversting = true;
    newsFeed = false;
    campaign = false;
    constructor() { }

    ngOnInit(): void {
    }

    toggleAdversiting() {
        this.adversting = true;
        this.newsFeed = this.campaign = false;
    }

    toggleCampaign() {
        this.campaign = true;
        this.adversting = this.newsFeed = false;
    }

    toggleNewsFeed() {
        this.newsFeed = true;
        this.campaign = this.adversting = false;
    }
}
