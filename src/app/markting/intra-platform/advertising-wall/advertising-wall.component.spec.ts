import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvertisingWallComponent } from './advertising-wall.component';

describe('AdvertisingWallComponent', () => {
  let component: AdvertisingWallComponent;
  let fixture: ComponentFixture<AdvertisingWallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertisingWallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertisingWallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
