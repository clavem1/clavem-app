import { Component, OnInit } from '@angular/core';
import { FetchActivePubsGQL, FetchNextPubsGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-advertising-wall',
  templateUrl: './advertising-wall.component.html',
  styleUrls: ['./advertising-wall.component.scss']
})
export class AdvertisingWallComponent implements OnInit {

    activePubs = [];
    nextPubs = [];
  constructor(
      private readonly fetchNextPubsGQL: FetchNextPubsGQL,
      private readonly fetchActivePubsGQL: FetchActivePubsGQL,
  ) { }

  ngOnInit(): void {
      this.fetchActivePubsGQL.fetch({}).subscribe(
          (result) => {
              this.activePubs = result.data.fetchActivePubs;
          }
      )
      this.fetchNextPubsGQL.fetch({}).subscribe(
          (result) => {
              this.nextPubs = result.data.fetchNextPubs;
          }
      )
  }

}
