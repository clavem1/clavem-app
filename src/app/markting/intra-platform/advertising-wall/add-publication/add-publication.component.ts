import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PublicationService } from './publication.service';

@Component({
    selector: 'app-add-publication',
    templateUrl: './add-publication.component.html',
    styleUrls: ['./add-publication.component.scss']
})
export class AddPublicationComponent implements OnInit {
    checked = false;
    dateNumber = 1;
    tabs = new Array(this.dateNumber);
    form: FormGroup;
    file;
    resultMessage: {message: string; status: string};
    constructor(
        private readonly publicationService: PublicationService,
    ) { }

    ngOnInit(): void {
        this.form = new FormGroup({
            title: new FormControl('', Validators.required),
            object: new FormControl(''),
            order: new FormControl(1, [Validators.min(1), Validators.required]),
            isUp: new FormControl(false, Validators.required),
            dependence: new FormControl(false, Validators.required),
            startDate: new FormControl(new Date(), Validators.required),
            startTime: new FormControl('00:00', Validators.required),
            endDate: new FormControl(new Date(), Validators.required),
            endTime: new FormControl('00:00', Validators.required),
        });
    }

    onChecked() {
        this.checked = !this.checked;
    }

    addDate() {
        this.tabs.length = this.dateNumber;
        console.log(this.tabs.length, this.dateNumber);
    }

    onPosterChange(event) {
        this.file = event.target.files[0];
    }

    createPub() {
        const formData = new FormData();
        const form = this.form.value;
        const controls = Object.keys(form);
        controls.map((control) => {
            formData.append(control, form[control]);
        });
        formData.append('file', this.file);
        this.publicationService.post(formData).subscribe(
            (pub) => {
                console.log(pub);
                this.resultMessage = { message: "Publication postée avec succès!", status: "success" };
            },
            (error) => {
                console.log(error);
                this.resultMessage = { message: "Erro! Veuillez remplir tous les champs svp!", status: "danger" };
            }
        )
    }
}
