import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AnyObject } from 'src/app/shared/typings/typescript';
import { environment } from 'src/environments/environment';

@Injectable()
export class PublicationService {
    constructor(private readonly httpClient: HttpClient) {}

    post(payload) {
        return this.httpClient.post(`${environment.API_URL}/pub`, payload);
    }
}
