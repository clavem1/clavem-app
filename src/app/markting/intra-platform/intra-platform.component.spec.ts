import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntraPlatformComponent } from './intra-platform.component';

describe('IntraPlatformComponent', () => {
  let component: IntraPlatformComponent;
  let fixture: ComponentFixture<IntraPlatformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntraPlatformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntraPlatformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
