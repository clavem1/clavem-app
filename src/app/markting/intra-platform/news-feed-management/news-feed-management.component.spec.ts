import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsFeedManagementComponent } from './news-feed-management.component';

describe('NewsFeedManagementComponent', () => {
  let component: NewsFeedManagementComponent;
  let fixture: ComponentFixture<NewsFeedManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewsFeedManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsFeedManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
