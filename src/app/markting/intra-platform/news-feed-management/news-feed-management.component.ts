import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-feed-management',
  templateUrl: './news-feed-management.component.html',
  styleUrls: ['./news-feed-management.component.scss']
})
export class NewsFeedManagementComponent implements OnInit {

  emplacementUp = true;
  other = false;

  constructor() { }

  ngOnInit(): void {
  }

  toggleEmplacement() {
    this.emplacementUp = true;
    this.other = false;
  }

  toggleOther() {
    this.other = true;
    this.emplacementUp = false;
  }

}
