import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddImageEmplacementUpComponent } from './add-image-emplacement-up.component';

describe('AddImageEmplacementUpComponent', () => {
  let component: AddImageEmplacementUpComponent;
  let fixture: ComponentFixture<AddImageEmplacementUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddImageEmplacementUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddImageEmplacementUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
