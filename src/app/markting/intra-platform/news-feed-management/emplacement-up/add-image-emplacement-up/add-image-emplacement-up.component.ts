import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PublicationService } from '../../../advertising-wall/add-publication/publication.service';

@Component({
  selector: 'app-add-image-emplacement-up',
  templateUrl: './add-image-emplacement-up.component.html',
  styleUrls: ['./add-image-emplacement-up.component.scss']
})
export class AddImageEmplacementUpComponent implements OnInit {
    form: FormGroup;
    file;
    resultMessage: { message: string; status: string };
    constructor(
        private readonly publicationService: PublicationService,
    ) { }

    ngOnInit(): void {
        this.form = new FormGroup({
            title: new FormControl('', Validators.required),
            object: new FormControl(''),
            order: new FormControl(1, [Validators.min(1), Validators.required]),
            isUp: new FormControl(true, Validators.required),
            dependence: new FormControl(false, Validators.required),
            startDate: new FormControl(new Date(), Validators.required),
            startTime: new FormControl('00:00', Validators.required),
            endDate: new FormControl(new Date(), Validators.required),
            endTime: new FormControl('00:00', Validators.required),
        });
    }

    onPosterChange(event) {
        this.file = event.target.files[0];
    }

    createPub() {
        const formData = new FormData();
        const form = this.form.value;
        const controls = Object.keys(form);
        controls.map((control) => {
            formData.append(control, form[control]);
        });
        formData.append('file', this.file);
        this.publicationService.post(formData).subscribe(
            (pub) => {
                console.log(pub);
                this.resultMessage = { message: "Publication postée avec succès!", status: "success" };
            },
            (error) => {
                console.log(error);
                this.resultMessage = { message: "Erro! Veuillez remplir tous les champs svp!", status: "danger" };
            }
        )
    }
}
