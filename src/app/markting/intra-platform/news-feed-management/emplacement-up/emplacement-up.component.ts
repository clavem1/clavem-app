import { Component, OnInit } from '@angular/core';
import { FetchUpActivePubsGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-emplacement-up',
  templateUrl: './emplacement-up.component.html',
  styleUrls: ['./emplacement-up.component.scss']
})
export class EmplacementUpComponent implements OnInit {

    pubs = [];
    constructor(private readonly fetchUpActivePubsGQL: FetchUpActivePubsGQL) { }

  ngOnInit(): void {
      this.fetchUpActivePubsGQL.fetch({}).subscribe(
          (result) => {
              this.pubs = result.data.fetchUpActivePubs;
          }
      )
  }

}
