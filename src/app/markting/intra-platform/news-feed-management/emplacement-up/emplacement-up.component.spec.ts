import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmplacementUpComponent } from './emplacement-up.component';

describe('EmplacementUpComponent', () => {
  let component: EmplacementUpComponent;
  let fixture: ComponentFixture<EmplacementUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmplacementUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmplacementUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
