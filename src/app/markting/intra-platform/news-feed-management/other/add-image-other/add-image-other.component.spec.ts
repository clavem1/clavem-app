import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddImageOtherComponent } from './add-image-other.component';

describe('AddImageOtherComponent', () => {
  let component: AddImageOtherComponent;
  let fixture: ComponentFixture<AddImageOtherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddImageOtherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddImageOtherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
