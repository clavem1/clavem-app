import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FetchNextNoUpPubsGQL, FetchNoUpActivePubsGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.scss']
})
export class OtherComponent implements OnInit {
    activePubs = [];
    nextPubs = [];
    baseAttachmentAPI = `${environment.API_URL}/attachment`;
  constructor(
      private readonly fetchNextNoUpPubsGQL: FetchNextNoUpPubsGQL,
      private readonly fetchNoUpActivePubsGQL: FetchNoUpActivePubsGQL,
  ) { }

  ngOnInit(): void {
      this.fetchNoUpActivePubsGQL.fetch({}).subscribe(
          (result) => {
              this.activePubs = result.data.fetchNoUpActivePubs;
          }
      )
      this.fetchNextNoUpPubsGQL.fetch({}).subscribe(
          (result) => {
              this.nextPubs = result.data.fetchNextNoUpPubs;
          }
      )

  }

    generateUrl(url) {
        return this.baseAttachmentAPI + '/' + url;
    }

}
