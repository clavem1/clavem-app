import { AuthService } from './../../auth/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { UserRole } from 'src/app/users/enums/user-role';
import { FetchEventsGQL, FetchEventGQL, ChangeEventStateGQL, ChangeEventStatusGQL, EventState, EventStatus } from 'src/generated/graphql';
import { AnyObject } from 'src/app/shared/typings/typescript';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-event-details',
    templateUrl: './event-details.component.html',
    styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit {
    event: AnyObject;
    eventId: string;
    userRole: UserRole;
    basePosterAPI = `${environment.API_URL}/event/poster`;
    updateResult: { status: string; message: string };
    availableStates = EventState;
    availableStatus = EventStatus;
    comments: boolean = false;
    users = [0, 1, 2, 3, 4]; //La liste users qui ont commentés
    userSelected = 0;

    constructor(
        private authService: AuthService,
        private readonly fetchEventGQL: FetchEventGQL,
        private readonly changeEventStateGQL: ChangeEventStateGQL,
        private readonly changeEventStatusGQL: ChangeEventStatusGQL,
        private readonly activatedRoute: ActivatedRoute,
    ) {
        this.getUserRole();
    }

    ngOnInit(): void {
        this.eventId = this.activatedRoute.snapshot.paramMap.get('id');
        this.fetchEventGQL.fetch({ eventId: this.eventId }).subscribe(
            (result) => {
                this.event = result.data.fetchEvent;
            }
        );
    }

    selectItem(index: number) {
        this.userSelected = this.users.indexOf(index)
        console.log(this.userSelected);

    }

    toggleComment() {
        this.comments = !this.comments;
    }

    getUserRole() {
        this.userRole = this.authService.getCurrentRole();
    }
    generateUrl(url) {
        return this.basePosterAPI + '/' + url;
    }

    changeState(state) {
        this.changeEventStateGQL.mutate({ eventId: this.eventId, state: state }).subscribe(
            (result) => {
                if (result.errors) {
                    this.updateResult = { 'status': 'danger', message: 'Erreur lors de la modification de l\'état!' };
                } else {
                    this.updateResult = { 'status': 'success', message: 'Etat modifié avec succés' };
                    this.event = result.data.changeEventState;
                }
            },
            (error) => {
                this.updateResult = { 'status': 'danger', message: 'Erreur lors de la modification de l\'état!' };
            }
        );
    }

    changeStatus(status) {
        this.changeEventStatusGQL.mutate({ eventId: this.eventId, status: status }).subscribe(
            (result) => {
                if (result.errors) {
                    this.updateResult = { 'status': 'danger', message: 'Erreur lors de la modification du statut!' };
                } else {
                    this.updateResult = { 'status': 'success', message: 'Statut modifié avec succés' };
                    this.event = result.data.changeEventStatus;
                }
            },
            (error) => {
                this.updateResult = { 'status': 'danger', message: 'Erreur lors de la modification du statut!' };
            }
        );
    }
}
