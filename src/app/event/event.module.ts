import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventRoutingModule } from './event-routing.module';
import { EventComponent } from './event.component';
import { TemplateModule } from '../shared/components/template/template.module';
import { EventDetailsComponent } from './event-details/event-details.component';
import { NewEventComponent } from './new-event/new-event.component';
import { EventFormComponent } from './event-form/event-form.component';
import { AccesCodeComponent } from './acces-code/acces-code.component';
import { CreateComponent } from './acces-code/create/create.component';
import { FormComponent } from './acces-code/form/form.component';
import { EventForm1Component } from './event-form/event-form1/event-form1.component';
import { EventForm2Component } from './event-form/event-form2/event-form2.component';
import { EventForm3Component } from './event-form/event-form3/event-form3.component';
import { EventForm4Component } from './event-form/event-form4/event-form4.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EventService } from './event.service';
import { AppInterceptor } from '../shared/interceptors/app.interceptor';

@NgModule({
  declarations: [
    EventComponent,
    EventDetailsComponent,
    NewEventComponent,
    EventFormComponent,
    AccesCodeComponent,
    CreateComponent,
    FormComponent,
    EventForm1Component,
    EventForm2Component,
    EventForm3Component,
    EventForm4Component
  ],
  providers: [
      EventService,
      {
          provide: HTTP_INTERCEPTORS,
          useClass: AppInterceptor,
          multi: true
      },
  ],
  imports: [
    CommonModule,
    EventRoutingModule,
    TemplateModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ]
})
export class EventModule { }
