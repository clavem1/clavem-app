import { Component, OnInit } from '@angular/core';
import result, { FetchAccessCodesGQL } from 'src/generated/graphql';

@Component({
    selector: 'app-acces-code',
    templateUrl: './acces-code.component.html',
    styleUrls: ['./acces-code.component.scss']
})
export class AccesCodeComponent implements OnInit {
    accessCodes;
    constructor(
        private readonly fetchAccessCodesGQL: FetchAccessCodesGQL,
    ) { }

    ngOnInit(): void {
        this.fetchAccessCodesGQL.fetch().subscribe(
            (result) => {
                this.accessCodes = result.data.fetchAccessCodes.records;
            }
        )
    }

}
