import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import result, { FetchAccessCodesGQL, CreateAccessCodeGQL } from 'src/generated/graphql';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
    form: FormGroup;
    createResult: { message: string; status: string };
    constructor(
        private readonly createAccessCodeGQL: CreateAccessCodeGQL,
    ) { }

    ngOnInit(): void {
        this.initForm();
    }

    initForm() {
        this.form = new FormGroup({
            username: new FormControl('', Validators.required),
            password: new FormControl('', Validators.required),
            role: new FormControl('ADMIN', Validators.required)
        });
    }

    submit() {
        this.createAccessCodeGQL.mutate({ accessCodeDto: this.form.value }).subscribe(
            (result) => {
                if (result.errors) {
                    this.createResult = { message: "Erreur lors de la création du code d'accés!", status: "danger" };
                } else {
                    this.createResult = { message: "Code d'accés créé avec succés!", status: "success" };
                }
            },
            (error) => {
                this.createResult = { message: "Erreur lors de la création du code d'accés!", status: "danger" };
            }
        )
    }
}
