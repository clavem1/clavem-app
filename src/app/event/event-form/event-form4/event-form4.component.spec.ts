import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventForm4Component } from './event-form4.component';

describe('EventForm4Component', () => {
  let component: EventForm4Component;
  let fixture: ComponentFixture<EventForm4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventForm4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventForm4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
