import { Component, OnInit, Output, Input } from '@angular/core';
import { AnyObject } from 'src/app/shared/typings/typescript';
import { EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-event-form4',
    templateUrl: './event-form4.component.html',
    styleUrls: ['./event-form4.component.scss']
})
export class EventForm4Component implements OnInit {
    @Output() formEmitter = new EventEmitter();
    @Input() formValue: AnyObject;
    form: FormGroup;
    formData: FormData;
    constructor() { }

    ngOnInit(): void {
        this.initForm();
    }

    initForm() {
        this.formData = new FormData();
    }

    onPosterChange(event) {
        const file = event.target.files[0];
        this.formData.append('poster', file);
        this.formEmitter.emit(this.formData);
    }

    onArchivesChange(event) {
        const files = event.target.files;
        for (const file of files) {
            this.formData.append('archives', file);
        }
        this.formEmitter.emit(this.formData);
    }
}
