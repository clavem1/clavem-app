import { Component, OnInit, Output, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AnyObject } from 'src/app/shared/typings/typescript';
import { EventEmitter } from '@angular/core';

@Component({
    selector: 'app-event-form3',
    templateUrl: './event-form3.component.html',
    styleUrls: ['./event-form3.component.scss']
})
export class EventForm3Component implements OnInit {
    @Output() formEmitter = new EventEmitter();
    @Input() formValue: AnyObject;
    form: FormGroup;
    constructor(private formBuilder: FormBuilder) { }

    ngOnInit(): void {
        this.initForm();
    }

    initForm() {
        this.form = new FormGroup({
            reservation: this.formBuilder.group({
                allowed: [true, Validators.required],
                payWhenReservation: [true, Validators.required],
                reservationFeeRefundable: [false, Validators.required],
                percentageToPay: [0, Validators.required],
                limiteDateConfirmation: [new Date(), Validators.required],
                limiteTimeConfirmation: ['00:00', Validators.required],
            }),
        });
        if (this.formValue) {
            this.form.setValue(this.formValue);
        }
        this.form.valueChanges.subscribe(
            (formValue) => {
                this.formEmitter.emit(this.form);
            }
        )
    }

    get reservation() { return this.form.controls.reservation as FormGroup }
}
