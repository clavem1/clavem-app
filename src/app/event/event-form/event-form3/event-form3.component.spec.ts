import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventForm3Component } from './event-form3.component';

describe('EventForm3Component', () => {
  let component: EventForm3Component;
  let fixture: ComponentFixture<EventForm3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventForm3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventForm3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
