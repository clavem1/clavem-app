import { Component, OnInit } from '@angular/core';
import { AnyObject } from 'src/app/shared/typings/typescript';
import { FormGroup } from '@angular/forms';
import { EventService } from '../event.service';

@Component({
    selector: 'app-event-form',
    templateUrl: './event-form.component.html',
    styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {
    form1 = true;
    form2 = false;
    form3 = false;
    form4 = false;
    widthProgress = '25%';
    progress = '75%';
    currentFormIsValid: boolean = false;
    generalFormValue: AnyObject;
    ticketFormValue: AnyObject;
    reservationFormValue: AnyObject;
    eventMultimediaFormValue: FormData;
    submitEventResult: { status: string; message: string };
    constructor(private readonly eventService: EventService) { }

    ngOnInit(): void {
    }

    toggleForm1() {
        this.form1 = true;
        this.progress = '75%';
        this.widthProgress = '25%';
        this.form2 = this.form3 = this.form4 = false;
    }
    toggleForm2() {
        this.form2 = true;
        this.progress = '50%';
        this.widthProgress = '50%';
        this.form1 = this.form3 = this.form4 = false;
    }
    toggleForm3() {
        this.form3 = true;
        this.progress = '25%';
        this.widthProgress = '75%';
        this.form2 = this.form1 = this.form4 = false;
    }
    toggleForm4() {
        this.form4 = true;
        this.form1 = this.form2 = this.form3 = false;
        this.progress = '0%';
        this.widthProgress = '100%';
    }

    previous() {
        if (this.form4 === true) {
            this.form4 = false;
            this.form3 = true;
            this.progress = '25%';
            this.widthProgress = '75%';
        } else if (this.form3 === true) {
            this.form3 = false;
            this.form2 = true;
            this.progress = '50%';
            this.widthProgress = '50%';
        } else {
            this.form2 = false;
            this.form1 = true;
            this.progress = '75%';
            this.widthProgress = '25%';
        }
    }

    next() {
        if (this.form1 === true) {
            this.form2 = true;
            this.form1 = false;
            this.progress = '50%';
            this.widthProgress = '50%';
        } else if (this.form2 === true) {
            this.form2 = false;
            this.form3 = true;
            this.progress = '25%';
            this.widthProgress = '75%';
        } else {
            this.form3 = false;
            this.form4 = true;
            this.progress = '0%';
            this.widthProgress = '100%';
        }
    }

    generalFormChanges(form: FormGroup) {
        this.generalFormValue = form.value;
        this.currentFormIsValid = form.valid;
    }

    ticketFormChanges(form: FormGroup) {
        this.ticketFormValue = form.value;
        this.currentFormIsValid = form.valid;
    }

    reservationFormChanges(form: FormGroup) {
        this.reservationFormValue = form.value;
        this.currentFormIsValid = form.valid;
    }

    eventMultimediaFormChanges(form: FormData) {
        this.eventMultimediaFormValue = form;
    }

    submitEvent() {
        const eventData: AnyObject = {
            ...this.generalFormValue,
            ...this.reservationFormValue,
            ...this.ticketFormValue,
        };
        const formData = this.eventMultimediaFormValue || new FormData();
        const fields = Object.keys(eventData);
        fields.map((field: string) => {
            formData.append(field, eventData[field]);
        });
        formData.append('formStringify', JSON.stringify(eventData));
        this.eventService.createEvent(formData).subscribe(
            (event) => {
                this.submitEventResult = { status: 'success', message: "Événement posté avec succés!" }
            },
            (error) => {
                this.submitEventResult = { status: 'danger', message: "Erreur lors de l'enregistrement de votre événement!" };
                console.warn(error);
            }
        );

    }
}
