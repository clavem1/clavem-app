import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventForm2Component } from './event-form2.component';

describe('EventForm2Component', () => {
  let component: EventForm2Component;
  let fixture: ComponentFixture<EventForm2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventForm2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventForm2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
