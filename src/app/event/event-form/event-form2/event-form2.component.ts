import { Component, OnInit, Output, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { AnyObject } from 'src/app/shared/typings/typescript';
import { EventEmitter } from '@angular/core';

@Component({
    selector: 'app-event-form2',
    templateUrl: './event-form2.component.html',
    styleUrls: ['./event-form2.component.scss']
})
export class EventForm2Component implements OnInit {
    @Output() formEmitter = new EventEmitter();
    @Input() formValue: AnyObject;
    form: FormGroup;
    checked = false;
    age = false;
    genre = false;
    allow = false;
    numberOfTickets = 0;
    prixRenseigner = [];
    tab = new Array(this.numberOfTickets);
    ticketingList = [];
    minimumAgeRequired: number = 0;

    constructor(private formBuilder: FormBuilder) { }

    ngOnInit(): void {
        this.initForm();
    }

    onChecked() {
        this.age = !this.age;
    }

    onCheckEntree() {
        this.checked = !this.checked;
    }

    onCheckedGenre() {
        this.genre = !this.genre;
    }

    addNumberOfTicket() {
        this.tab.length = this.numberOfTickets;
    }

    onAllow() {
        this.allow = this.offerOnTicketsPurchases.value.purchasedTicketInvolveFreeTicket;
    }

    initForm() {
        this.form = this.formBuilder.group({
            paidEntrance: [true, Validators.required],
            priceIncludingCharges: [true, Validators.required],
            categoryCriteria: new FormArray([]),
            numberOfTickets: [0, Validators.required],
            tickets: new FormArray([]),
            offerOnTicketsPurchases: this.formBuilder.group({
                purchasedTicketInvolveFreeTicket: [false],
                purchasedTickets: this.formBuilder.group({
                    quantity: [0, [Validators.required, Validators.min(1)]],
                    categoryCriteria: [null, [Validators.required, Validators.min(1)]]
                }),
                offeredTickets: this.formBuilder.group({
                    quantity: [0, [Validators.required, Validators.min(1)]],
                    categoryCriteria: [null, [Validators.required, Validators.min(1)]]
                }),
            })
        });
        // this.formEmitter.emit(this.form);
        /*if (this.formValue) {
            console.log('FORM 2 ', this.formValue);
            setTimeout(() => {
                this.form.patchValue(this.formValue);
            }, 1000);
        }*/
        this.form.valueChanges.subscribe(
            (formValue) => {
                this.formEmitter.emit(this.form);
            }
        )
    }


    onChangeTickets(e) {
        const numberOfTickets = e.target.value || 0;
        if (this.tickets.length < numberOfTickets) {
            for (let i = this.tickets.length; i < numberOfTickets; i++) {
                this.tickets.push(this.formBuilder.group({
                    name: ['', Validators.required],
                    categoryCriteria: ['', [Validators.required]],
                    quantity: ['', Validators.required],
                    price: ['', Validators.required]
                }));
            }
        } else {
            for (let i = this.tickets.length; i >= numberOfTickets; i--) {
                this.tickets.removeAt(i);
            }
        }
    }

    onEventCategoriesChange(event) {
        const categories: FormArray = this.form.get('categoryCriteria') as FormArray;
        const category = event.target.value;
        if (event.target.checked) {
            categories.push(new FormControl(category));
        }
        else {
            let i: number = 0;

            categories.controls.forEach((ctrl: FormControl) => {
                if (ctrl.value == category) {
                    categories.removeAt(i);
                    return;
                }
                i++;
            });
        }
        this[category] = !this[category];
    }

    minimumAgeRequiredChange(event) {
        this.minimumAgeRequired = event.target.value;
    }

    // convenience getters for easy access to form fields
    get formControls() { return this.form.controls; }
    get tickets() { return this.formControls.tickets as FormArray; }
    get numberOftickets() { return this.formControls.numberOftickets.value; }
    get categoryCriteria() { return this.formControls.categoryCriteria.value; }
    get offerOnTicketsPurchases() { return this.formControls.offerOnTicketsPurchases as FormGroup }
    get purchasedTickets() { return this.offerOnTicketsPurchases.controls.purchasedTickets }
    get offeredTickets() { return this.offerOnTicketsPurchases.controls.offeredTickets }

    eventContainsCategoryCriteria(criteria: string) {
        return this.categoryCriteria.indexOf(criteria) >= 0;
    }

    hasOfferOnTicketPurchases() {
        return this.offerOnTicketsPurchases.value.purchasedTicketInvolveFreeTicket;
    }
}
