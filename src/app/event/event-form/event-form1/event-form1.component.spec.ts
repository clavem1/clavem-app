import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventForm1Component } from './event-form1.component';

describe('EventForm1Component', () => {
  let component: EventForm1Component;
  let fixture: ComponentFixture<EventForm1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventForm1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventForm1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
