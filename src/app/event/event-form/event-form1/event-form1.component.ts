import { Component, OnInit, Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EventType, FetchCategoriesGQL, CategoriesEntity, EventAccessType } from 'src/generated/graphql';
import { AnyObject } from 'src/app/shared/typings/typescript';

@Component({
    selector: 'app-event-form1',
    templateUrl: './event-form1.component.html',
    styleUrls: ['./event-form1.component.scss']
})
export class EventForm1Component implements OnInit {
    @Output() formEmitter = new EventEmitter();
    @Input() formValue: AnyObject;
    form: FormGroup;
    eventType = EventType;
    categories;
    accessType = EventAccessType;
    constructor(private fetchCategoriesGQL: FetchCategoriesGQL) { }

    ngOnInit(): void {
        this.initForm();
        this.fetchCategoriesGQL.fetch({}).subscribe(
            (result) => {
                this.categories = result.data.fetchCategories;
            }
        );
    }

    initForm() {
        this.form = new FormGroup({
            name: new FormControl('', Validators.required),
            type: new FormControl('', Validators.required),
            category: new FormControl('', Validators.required),
            description: new FormControl('', Validators.required),
            catchyPhrase: new FormControl('', Validators.required),
            startDate: new FormControl(new Date(), Validators.required),
            endDate: new FormControl(new Date, Validators.required),
            startTime: new FormControl('00:00', Validators.required),
            endTime: new FormControl('00:00', Validators.required),
            address: new FormControl('', Validators.required),
            locationAccuracy: new FormControl('', Validators.required),
            expectedNumberOfPersons: new FormControl('', Validators.required),
            accessType: new FormControl('', Validators.required),
            keepContactWithParticipant: new FormControl(true, Validators.required),
        });
        if (this.formValue) {
            this.form.setValue(this.formValue);
        }
        this.form.valueChanges.subscribe(
            (formValue) => {
                this.formEmitter.emit(this.form);
            }
        )
    }
}
