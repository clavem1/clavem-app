import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class EventService {
    constructor(private readonly httpClient: HttpClient) { }

    createEvent(event: FormData) {
        return this.httpClient.post(`${environment.API_URL}/event/create`, event);
    }
}
