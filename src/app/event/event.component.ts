import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/service/auth.service';
import { UserRole } from '../users/enums/user-role';
import { FetchCategoryEventsGQL } from 'src/generated/graphql';
import { AnyObject } from '../shared/typings/typescript';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-event',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
    basePosterAPI = `${environment.API_URL}/event/poster`;
    events: AnyObject[];
    userRole: UserRole;
    categoryId: string;
    constructor(
        private authService: AuthService,
        private readonly fetchCategoryEventsGQL: FetchCategoryEventsGQL,
        private readonly activatedRoute: ActivatedRoute,
    ) { }

    ngOnInit(): void {
        this.getUserRole();
        this.categoryId = this.activatedRoute.snapshot.paramMap.get('id');
        this.fetchCategoryEventsGQL.fetch({ categoryId: this.categoryId }).subscribe(
            (result) => {
                this.events = result.data.fetchCategoryEvents.records;
            }
        );
    }

    getUserRole() {
        this.userRole = this.authService.getCurrentRole();
    }

    generateUrl(url) {
        return this.basePosterAPI + '/' + url;
    }

}
