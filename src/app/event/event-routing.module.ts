import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventComponent } from './event.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { NewEventComponent } from './new-event/new-event.component';
import { AccesCodeComponent } from './acces-code/acces-code.component';
import { CreateComponent } from './acces-code/create/create.component';


const routes: Routes = [
    { path: '', component: EventComponent },
    { path: 'new-event', component: NewEventComponent },
    { path: 'acces-code', component: AccesCodeComponent },
    { path: 'acces-code/create', component: CreateComponent },
    { path: ':id', component: EventDetailsComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EventRoutingModule { }
