import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UsersListComponent } from './users-list/users-list.component';
import { TemplateModule } from '../shared/components/template/template.module';
import { NewUserComponent } from './new-user/new-user.component';
import { UserFormComponent } from './user-form/user-form.component';
import { DetailsComponent } from './details/details.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [UsersComponent, UsersListComponent, NewUserComponent, UserFormComponent, DetailsComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    TemplateModule,
    FormsModule
  ]
})
export class UsersModule { }
