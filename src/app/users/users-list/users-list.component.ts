import { Component, OnInit } from '@angular/core';
import { FetchClientsGQL, CloseAccountGQL } from 'src/generated/graphql';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/auth/service/auth.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  clients = [];
  clientsList = [];
  constructor(
    private readonly fetchClientsGQL: FetchClientsGQL,
  ) { }

  ngOnInit(): void {
    this.fetchClientsGQL.fetch({}).subscribe(
      (result) => {
        this.clients = result.data.fetchClients.records;
        this.clientsList = this.clients;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  onSearch(value: NgForm) {
    this.clientsList = this.clients;
    console.log(value)
    if (value['search'] == "") {
      this.ngOnInit();
    }
    if (value['dateDebut'] === "" && value['dateFin'] === "") {
      this.clientsList = this.clientsList.filter(client => {
        if (client.firstName.toLowerCase().match(value['search']) !== null) {
          return true;
        }
        if (client.lastName.toLowerCase().match(value['search']) !== null) {
          return true;
        }
        if (client.email.match(value['search']) !== null) {
          return true;
        }
        if (client.phoneNumber.match(value['search']) !== null) {
          return true;
        }
        if (client.city !== null) {
          if (client.city.toLowerCase().match(value['search'].toLowerCase()) !== null) {
            return true;
          }
        }

        if (new Date(client.birthDate).toLocaleDateString().match(String(value['search'])) !== null) {
          return true;
        }
      });
    }
  }

  

}
