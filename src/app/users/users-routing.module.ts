import { NewUserComponent } from './new-user/new-user.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';
import {DetailsComponent} from './details/details.component';


const routes: Routes = [
  { path: '', component: UsersListComponent },
  { path: 'new-user', component: NewUserComponent },
  { path: 'details', component: DetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
