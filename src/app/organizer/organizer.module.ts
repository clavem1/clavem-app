import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganizerRoutingModule } from './organizer-routing.module';
import { OrganizerComponent } from './organizer.component';
import { TemplateModule } from '../shared/components/template/template.module';
import { OrganizerService } from './service/organizer.service';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule } from '@angular/forms';
import { DetailComponent } from './detail/detail.component';


@NgModule({
  declarations: [OrganizerComponent, DetailComponent],
  imports: [
    CommonModule,
    OrganizerRoutingModule,
    TemplateModule,
    Ng2SearchPipeModule,
    FormsModule,
  ],
  providers: [
    OrganizerService
  ]
})
export class OrganizerModule { }
