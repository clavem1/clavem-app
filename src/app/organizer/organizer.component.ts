import { Component, OnInit } from '@angular/core';
import { OrganizerService } from './service/organizer.service';
import { IUser } from '../users/interfaces/user.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { IFindManyResult } from '../shared/typings/find-many-result.interface';
import { IClientFilter } from '../shared/typings/client-filter-payload.interface';
import { CloseAccountGQL, FetchOrganizersGQL } from 'src/generated/graphql';
import { AuthService } from '../auth/service/auth.service';

@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.scss']
})
export class OrganizerComponent implements OnInit {
  organizers;
  pages: number[];
  searchText: string;
  clientFilter: IClientFilter = {
    offset: 0
  };

  constructor(
    private readonly organizerService: OrganizerService,
    private readonly fetchOrganizersGQL: FetchOrganizersGQL,
    private readonly closeAccountGQL: CloseAccountGQL,
    private readonly authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.fetchOrgainizers(1);
  }

  fetchOrgainizers(page: number) {
    this.clientFilter.offset = (page - 1) * 10;
    this.fetchOrganizersGQL.fetch().subscribe(
      (result) => {
        this.organizers = result.data.fetchOrganizers;
        console.log(this.organizers.records)
        this.pages = Array(this.organizers.length).fill(0);
      }
    )
  }

  closeAccount(userId, userIndex) {
    const currentSession = this.authService.getCurrentSession();
    this.closeAccountGQL.mutate({ userId: currentSession.user._id }).subscribe(
      (result) => {
        if(result.errors) {
          alert('Une erreur est survenue!');
        } else {
          this.organizers.records[userIndex].state = "CLOSED";
        }
      }
    )
  }
}
