import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/users/interfaces/user.interface';
import { IFindManyResult } from 'src/app/shared/typings/find-many-result.interface';
import { IClientFilter } from 'src/app/shared/typings/client-filter-payload.interface';

@Injectable()
export class OrganizerService {
    constructor(private readonly http: HttpClient) { }

    fetchOrganizers(clientFilter: IClientFilter = {}): Observable<IFindManyResult<IUser>> {
        let params = new HttpParams();
        Object.keys(clientFilter).forEach(function (key) {
            params = params.append(key, clientFilter[key]);
        });
        return this.http.get<IFindManyResult<IUser>>(`${environment.API_URL}/organizers`, { params });
    }
}