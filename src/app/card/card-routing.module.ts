import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCommandComponent } from './list-command/list-command.component';
import { ListCardComponent } from './list-card/list-card.component';


const routes: Routes = [
  { path: 'list-card', component: ListCardComponent },
  { path: 'list-command', component: ListCommandComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardRoutingModule { }
