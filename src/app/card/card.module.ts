import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardRoutingModule } from './card-routing.module';
import { CardComponent } from './card.component';
import { TemplateModule } from '../shared/components/template/template.module';
import { ListCommandComponent } from './list-command/list-command.component';
import { ListCardComponent } from './list-card/list-card.component';


@NgModule({
  declarations: [CardComponent, ListCommandComponent, ListCardComponent],
  imports: [
    CommonModule,
    CardRoutingModule,
    TemplateModule
  ]
})
export class CardModule { }
