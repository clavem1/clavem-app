import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/guard/auth.guard';


const routes: Routes = [
    { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
    {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'users',
        loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.CategoryModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'card',
        loadChildren: () => import('./card/card.module').then(m => m.CardModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'organizer',
        loadChildren: () => import('./organizer/organizer.module').then(m => m.OrganizerModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'event',
        loadChildren: () => import('./event/event.module').then(m => m.EventModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
    },
    {
        path: 'admins', loadChildren: () => import('./settings/admins.module').then(m => m.AdminsModule)
    },
    {
        path: 'finance', loadChildren: () => import('./finance/finance.module').then(m => m.FinanceModule)
    },
    {
        path: 'communication', loadChildren: () => import('./markting/markting.module').then(m => m.MarktingModule)
    },
    {
        path: 'ticketing', loadChildren: () => import('./ticketing/ticketing.module').then(m => m.TicketingModule)
    },
    {
        path: 'event/:id/participant', loadChildren: () => import('./participant/participant.module').then(m => m.ParticipantModule)
    },
    {
        path: 'promotional-code', loadChildren: () => import('./promotional-code/promotional-code.module').then(m => m.PromotionalCodeModule)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
