import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/service/auth.service';
import { Router } from '@angular/router';
import { FetchCurrentUserGQL } from 'src/generated/graphql';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    baseAvatarAPI = `${environment.API_URL}/multimedia/images/avatar`;
    currentUser;
    constructor(
        private readonly authService: AuthService,
        private readonly router: Router,
        private readonly fetchCurrentUserGQL: FetchCurrentUserGQL,
    ) { }

    ngOnInit(): void {
        this.fetchCurrentUserGQL.fetch().subscribe(
            (result) => {
                this.currentUser = result.data.fetchCurrentUser;
            }
        )
    }

    logout() {
        this.authService.logout();
        this.router.navigate(['/auth/login']);
    }

    generateUrl(url) {
        return this.baseAvatarAPI + '/' + url;
    }
}
