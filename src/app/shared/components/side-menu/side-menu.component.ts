import { AuthService } from './../../../auth/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { UserRole } from 'src/app/users/enums/user-role';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  userRole: UserRole;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.getUserRole();
  }

 getUserRole() {
    this.userRole = this.authService.getCurrentRole();
  }


}
