import gql from 'graphql-tag';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The javascript `Date` as string. Type represents date and time as the ISO Date string. */
  DateTime: any;
  /** Arbitrary object */
  Any: any;
};



export type AccessCodeDto = {
  username: Scalars['String'];
  password: Scalars['String'];
  role: AccessCodeRole;
};

export type AccessCodeEntity = {
  __typename?: 'AccessCodeEntity';
  _id: Scalars['ID'];
  organizer: UserEntity;
  username: Scalars['String'];
  password: Scalars['String'];
  role: AccessCodeRole;
};

export enum AccessCodeRole {
  Admin = 'ADMIN',
  Editor = 'EDITOR',
  ParticipantsManager = 'PARTICIPANTS_MANAGER',
  Observer = 'OBSERVER'
}

export type AccessCodesEntity = {
  __typename?: 'AccessCodesEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<AccessCodeEntity>;
};


export type AttachmentRecord = {
  __typename?: 'AttachmentRecord';
  id: Scalars['ID'];
  sizeB: Scalars['Int'];
  extension: Scalars['String'];
};

export type CardEntity = {
  __typename?: 'CardEntity';
  _id: Scalars['ID'];
  number: Scalars['String'];
  owner: UserEntity;
};

export type CardsEntity = {
  __typename?: 'CardsEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<CardEntity>;
};

export type CategoriesEntity = {
  __typename?: 'CategoriesEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<CategoryEntity>;
};

export type CategoryEntity = {
  __typename?: 'CategoryEntity';
  _id: Scalars['ID'];
  name: Scalars['String'];
  refused: Scalars['Float'];
  validated: Scalars['Float'];
  blocked: Scalars['Float'];
  pending: Scalars['Float'];
  activated: Scalars['Float'];
  desactivated: Scalars['Float'];
  archived: Scalars['Float'];
  paidEntrance: Scalars['Float'];
  freeEntrance: Scalars['Float'];
};

export type CategoryInput = {
  name: Scalars['String'];
};

export type ClientFilterInput = {
  offset?: Maybe<Scalars['Int']>;
  limit?: Maybe<Scalars['Int']>;
  filter?: Maybe<Scalars['Any']>;
  search?: Maybe<Scalars['String']>;
  orderBy?: Maybe<OrderByInput>;
};

export type CommentDto = {
  message: Scalars['String'];
  destinataire: Scalars['ID'];
  event: Scalars['ID'];
};

export type CommentEntity = {
  __typename?: 'CommentEntity';
  id: Scalars['ID'];
  message: Scalars['String'];
  createdAt: Scalars['DateTime'];
  sender: UserEntity;
  destinataire: UserEntity;
  event: EventEntity;
};


export enum EventAccessType {
  Uniq = 'Uniq',
  Free = 'Free'
}

export type EventDto = {
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  locationAccuracy?: Maybe<Scalars['String']>;
  type?: Maybe<EventType>;
  name?: Maybe<Scalars['String']>;
  catchyPhrase?: Maybe<Scalars['String']>;
  category?: Maybe<Scalars['ID']>;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  expectedNumberOfPersons?: Maybe<Scalars['Float']>;
  accessType?: Maybe<EventAccessType>;
  keepContactWithParticipant?: Maybe<Scalars['Boolean']>;
  paidEntrance?: Maybe<Scalars['Boolean']>;
};

export type EventEntity = {
  __typename?: 'EventEntity';
  _id: Scalars['ID'];
  description?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  locationAccuracy?: Maybe<Scalars['String']>;
  state?: Maybe<EventState>;
  status?: Maybe<EventStatus>;
  type?: Maybe<EventType>;
  name?: Maybe<Scalars['String']>;
  catchyPhrase?: Maybe<Scalars['String']>;
  createdBy: UserEntity;
  category: CategoryEntity;
  startDate?: Maybe<Scalars['DateTime']>;
  endDate?: Maybe<Scalars['DateTime']>;
  expectedNumberOfPersons?: Maybe<Scalars['Float']>;
  accessType?: Maybe<EventAccessType>;
  keepContactWithParticipant?: Maybe<Scalars['Boolean']>;
  paidEntrance?: Maybe<Scalars['Boolean']>;
  priceIncludingCharges?: Maybe<Scalars['Boolean']>;
  categoryCriteria?: Maybe<Array<Scalars['String']>>;
  purchasedTicketInvolveFreeTicket?: Maybe<TicketRequirements>;
  tickets?: Maybe<Array<Ticket>>;
  reservation?: Maybe<ReservationRequirements>;
  poster?: Maybe<ImageSizes>;
};

export type EventsEntity = {
  __typename?: 'EventsEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<EventEntity>;
};

export enum EventState {
  Activated = 'ACTIVATED',
  Desactivated = 'DESACTIVATED',
  Archived = 'ARCHIVED'
}

export enum EventStatus {
  Validated = 'VALIDATED',
  Refused = 'REFUSED',
  Pending = 'PENDING',
  Blocked = 'BLOCKED'
}

export enum EventType {
  Private = 'Private',
  Public = 'Public'
}

export type ImageSizes = {
  __typename?: 'ImageSizes';
  sm?: Maybe<Scalars['ID']>;
  md?: Maybe<Scalars['ID']>;
  lg?: Maybe<Scalars['ID']>;
};

export enum InternalRole {
  Strategic = 'STRATEGIC',
  Finance = 'FINANCE',
  Marketing = 'MARKETING',
  Standard = 'STANDARD'
}

export type LoginDto = {
  email: Scalars['String'];
  password: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createComment: CommentEntity;
  changeEventState: EventEntity;
  changeEventStatus: EventEntity;
  createTicket: EventEntity;
  removeTicket: EventEntity;
  updateEvent: EventEntity;
  updateCurrentUserPassword: Scalars['Boolean'];
  closeAccount: Scalars['Boolean'];
  activateAccount: Scalars['Boolean'];
  createCard: CardEntity;
  lostCard: CardEntity;
  register: SessionEntity;
  updatePassword: Scalars['Boolean'];
  createCategory: CategoryEntity;
  createPromotionalCode: PromotionalCodeEntity;
  createAccessCode: AccessCodeEntity;
  createReservation: Scalars['Boolean'];
};


export type MutationCreateCommentArgs = {
  commentInput: CommentDto;
};


export type MutationChangeEventStateArgs = {
  state: EventState;
  eventId: Scalars['ID'];
};


export type MutationChangeEventStatusArgs = {
  status: EventStatus;
  eventId: Scalars['ID'];
};


export type MutationCreateTicketArgs = {
  ticketDto: TicketDto;
  eventId: Scalars['ID'];
};


export type MutationRemoveTicketArgs = {
  ticketId: Scalars['ID'];
  eventId: Scalars['ID'];
};


export type MutationUpdateEventArgs = {
  eventInput: EventDto;
  eventId: Scalars['ID'];
};


export type MutationUpdateCurrentUserPasswordArgs = {
  newPassword: Scalars['String'];
  oldPassword: Scalars['String'];
};


export type MutationCloseAccountArgs = {
  userId?: Maybe<Scalars['ID']>;
};


export type MutationActivateAccountArgs = {
  userId: Scalars['ID'];
};


export type MutationCreateCardArgs = {
  cardNumber: Scalars['String'];
};


export type MutationLostCardArgs = {
  emergencyCard?: Maybe<Scalars['String']>;
  cardNumber: Scalars['String'];
};


export type MutationRegisterArgs = {
  registerInput: RegisterDto;
};


export type MutationUpdatePasswordArgs = {
  updatePasswordDto: UpdatePasswordDto;
};


export type MutationCreateCategoryArgs = {
  categoryInput: CategoryInput;
};


export type MutationCreatePromotionalCodeArgs = {
  promotionalCodeDto: PromotionalCodeDto;
};


export type MutationCreateAccessCodeArgs = {
  accessCodeDto: AccessCodeDto;
};


export type MutationCreateReservationArgs = {
  reservationInput: ReservationInput;
};

/** OrderBy direction */
export enum OrderByDirection {
  Asc = 'Asc',
  Desc = 'Desc'
}

export type OrderByInput = {
  property: Scalars['String'];
  direction: OrderByDirection;
};

export type Paging = {
  __typename?: 'Paging';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
};

export type PreferenceDto = {
  category: Scalars['ID'];
  startDate: Scalars['DateTime'];
  endDate: Scalars['DateTime'];
  city: Scalars['String'];
  region: Scalars['String'];
  price: Scalars['Float'];
};

export type PromotionalCodeDto = {
  eventId: Scalars['ID'];
  impact: PromotionalCodeImpact;
  privateName: Scalars['String'];
  numberOfGeneratedCodes: Scalars['Float'];
  tickets: Array<Scalars['ID']>;
  reductionEffect: ReductionEffectDto;
  publicName: Scalars['String'];
  usableNumberOfTimes: Scalars['Float'];
  startDate: Scalars['DateTime'];
  endDate: Scalars['DateTime'];
};

export type PromotionalCodeEntity = {
  __typename?: 'PromotionalCodeEntity';
  _id: Scalars['String'];
  eventId: Scalars['ID'];
  event: EventEntity;
  impact: PromotionalCodeImpact;
  privateName: Scalars['String'];
  numberOfGeneratedCodes: Scalars['Float'];
  tickets: Array<Ticket>;
  reductionEffect: ReductionEffectEntity;
  publicName: Scalars['String'];
  usableNumberOfTimes: Scalars['Float'];
  startDate: Scalars['DateTime'];
  endDate: Scalars['DateTime'];
};

export enum PromotionalCodeImpact {
  MaskTickets = 'MASK_TICKETS',
  ApplyReduction = 'APPLY_REDUCTION'
}

export type PromotionalCodesEntity = {
  __typename?: 'PromotionalCodesEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<PromotionalCodeEntity>;
};

export type PubEntity = {
  __typename?: 'PubEntity';
  _id: Scalars['ID'];
  title: Scalars['String'];
  object: Scalars['String'];
  startDate: Scalars['DateTime'];
  endDate: Scalars['DateTime'];
  isUp: Scalars['Boolean'];
  dependence: Scalars['String'];
  attachment: Scalars['ID'];
  order: Scalars['Float'];
  createdAt: Scalars['DateTime'];
};

export type Query = {
  __typename?: 'Query';
  fetchComments: Array<CommentEntity>;
  fetchClientComments: Array<CommentEntity>;
  fetchEvents: EventsEntity;
  fetchEvent: EventEntity;
  fetchRecentEvents: EventsEntity;
  fetchCategoryEvents: EventsEntity;
  fetchPreferences: Array<EventEntity>;
  fetchCurrentUser: UserEntity;
  fetchAdmins: UsersEntity;
  fetchOrganizers: UsersEntity;
  fetchClients: UsersEntity;
  fetchCards: CardsEntity;
  fetchMyCards: Array<CardEntity>;
  fetchPubs: Array<PubEntity>;
  fetchUpPubs: Array<PubEntity>;
  fetchNoUpPubs: Array<PubEntity>;
  fetchActivePubs: Array<PubEntity>;
  fetchNextPubs: Array<PubEntity>;
  fetchUpActivePubs: Array<PubEntity>;
  fetchNoUpActivePubs: Array<PubEntity>;
  fetchNextUpPubs: Array<PubEntity>;
  fetchNextNoUpPubs: Array<PubEntity>;
  login: SessionEntity;
  resetPassword: Scalars['String'];
  fetchCategories: CategoriesEntity;
  fetchPromotionalCodes: PromotionalCodesEntity;
  fetchAccessCodes: AccessCodesEntity;
  fetchMyReservations: Array<ReservationEntity>;
  fetchEventReservations: Array<ReservationEntity>;
};


export type QueryFetchEventArgs = {
  eventId: Scalars['ID'];
};


export type QueryFetchCategoryEventsArgs = {
  categoryId: Scalars['ID'];
};


export type QueryFetchPreferencesArgs = {
  preferenceInput: PreferenceDto;
};


export type QueryLoginArgs = {
  loginInput: LoginDto;
};


export type QueryResetPasswordArgs = {
  email: Scalars['String'];
};


export type QueryFetchEventReservationsArgs = {
  eventId: Scalars['ID'];
};

export type ReductionEffectDto = {
  inPercentage: Scalars['Boolean'];
  reduction: Scalars['Float'];
};

export type ReductionEffectEntity = {
  __typename?: 'ReductionEffectEntity';
  inPercentage: Scalars['Boolean'];
  reduction: Scalars['Float'];
};

export type RegisterDto = {
  phoneNumber: Scalars['String'];
  lastName: Scalars['String'];
  firstName: Scalars['String'];
  password: Scalars['String'];
  email: Scalars['String'];
  birthDate: Scalars['DateTime'];
  gender: Scalars['String'];
  city: Scalars['String'];
  district: Scalars['String'];
};

export type ReservationEntity = {
  __typename?: 'ReservationEntity';
  _id: Scalars['ID'];
  card: CardEntity;
  ticket: Ticket;
  client: UserEntity;
  state: ReservationState;
  event: EventEntity;
  createdAt: Scalars['DateTime'];
};

export type ReservationInput = {
  card?: Maybe<Scalars['String']>;
  ticket?: Maybe<Scalars['String']>;
  cardForOther?: Maybe<Scalars['String']>;
  ticketForOther?: Maybe<Scalars['String']>;
};

export type ReservationRequirements = {
  __typename?: 'ReservationRequirements';
  allowed: Scalars['Boolean'];
  payWhenReservation: Scalars['Boolean'];
  reservationFeeRefundable: Scalars['Boolean'];
  percentageToPay: Scalars['Float'];
  limiteDateConfirmation: Scalars['DateTime'];
};

export enum ReservationState {
  Accepted = 'ACCEPTED',
  Refused = 'REFUSED',
  Pending = 'PENDING'
}

export type SessionEntity = {
  __typename?: 'SessionEntity';
  token: Scalars['String'];
  user: UserEntity;
};

export type Ticket = {
  __typename?: 'Ticket';
  _id: Scalars['ID'];
  name: Scalars['String'];
  categoryCriteria: Scalars['String'];
  quantity: Scalars['Float'];
  price: Scalars['Float'];
};

export type TicketDto = {
  name: Scalars['String'];
  categoryCriteria: Scalars['String'];
  quantity: Scalars['Float'];
  price: Scalars['Float'];
};

export type TicketRequirements = {
  __typename?: 'TicketRequirements';
  purchasedTicketInvolveFreeTicket: Scalars['Boolean'];
  purchasedTickets: TicketRequirementsQuantity;
  offeredTickets: TicketRequirementsQuantity;
};

export type TicketRequirementsQuantity = {
  __typename?: 'TicketRequirementsQuantity';
  quantity: Scalars['Float'];
  categoryCriteria: Scalars['String'];
};

export type UpdatePasswordDto = {
  password: Scalars['String'];
  resetToken: Scalars['String'];
  resetCode: Scalars['Float'];
};

export type UserDto = {
  countryCode: Scalars['String'];
  role: UserRoles;
  gender: UserGender;
  birthDate: Scalars['DateTime'];
  phoneNumber: Scalars['String'];
  lastName: Scalars['String'];
  firstName: Scalars['String'];
  email: Scalars['String'];
  internalRole: InternalRole;
};

export type UserEntity = {
  __typename?: 'UserEntity';
  _id: Scalars['ID'];
  countryCode?: Maybe<Scalars['String']>;
  role: UserRoles;
  internalRole?: Maybe<InternalRole>;
  gender?: Maybe<UserGender>;
  birthDate?: Maybe<Scalars['DateTime']>;
  phoneNumber?: Maybe<Scalars['String']>;
  lastName: Scalars['String'];
  firstName: Scalars['String'];
  password: Scalars['String'];
  email: Scalars['String'];
  avatar?: Maybe<ImageSizes>;
  city?: Maybe<Scalars['String']>;
  district?: Maybe<Scalars['String']>;
  cards: Array<CardEntity>;
  state: UserState;
};

export enum UserGender {
  Male = 'MALE',
  Female = 'FEMALE'
}

export enum UserRoles {
  User = 'USER',
  Admin = 'ADMIN',
  Organizer = 'ORGANIZER'
}

export type UsersEntity = {
  __typename?: 'UsersEntity';
  recordsLength: Scalars['Float'];
  totalRecords: Scalars['Float'];
  offset: Scalars['Float'];
  limit: Scalars['Float'];
  pages: Scalars['Float'];
  currentPage: Scalars['Float'];
  records: Array<UserEntity>;
};

export enum UserState {
  Closed = 'CLOSED',
  Fonctionnal = 'FONCTIONNAL'
}

export type FetchCategoriesQueryVariables = {};


export type FetchCategoriesQuery = (
  { __typename?: 'Query' }
  & { fetchCategories: (
    { __typename?: 'CategoriesEntity' }
    & Pick<CategoriesEntity, 'limit' | 'recordsLength' | 'totalRecords' | 'offset' | 'pages' | 'currentPage'>
    & { records: Array<(
      { __typename?: 'CategoryEntity' }
      & Pick<CategoryEntity, '_id' | 'name' | 'validated' | 'refused' | 'pending' | 'activated' | 'desactivated' | 'archived' | 'paidEntrance' | 'freeEntrance'>
    )> }
  ) }
);

export type CreateCategoryMutationVariables = {
  categoryInput: CategoryInput;
};


export type CreateCategoryMutation = (
  { __typename?: 'Mutation' }
  & { createCategory: (
    { __typename?: 'CategoryEntity' }
    & Pick<CategoryEntity, '_id' | 'name'>
  ) }
);

export type FetchAccessCodesQueryVariables = {};


export type FetchAccessCodesQuery = (
  { __typename?: 'Query' }
  & { fetchAccessCodes: (
    { __typename?: 'AccessCodesEntity' }
    & { records: Array<(
      { __typename?: 'AccessCodeEntity' }
      & Pick<AccessCodeEntity, '_id' | 'username' | 'password' | 'role'>
      & { organizer: (
        { __typename?: 'UserEntity' }
        & Pick<UserEntity, 'firstName' | 'lastName'>
      ) }
    )> }
  ) }
);

export type CreateAccessCodeMutationVariables = {
  accessCodeDto: AccessCodeDto;
};


export type CreateAccessCodeMutation = (
  { __typename?: 'Mutation' }
  & { createAccessCode: (
    { __typename?: 'AccessCodeEntity' }
    & Pick<AccessCodeEntity, '_id' | 'username' | 'password' | 'role'>
    & { organizer: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, 'firstName' | 'lastName'>
    ) }
  ) }
);

export type FetchEventsQueryVariables = {};


export type FetchEventsQuery = (
  { __typename?: 'Query' }
  & { fetchEvents: (
    { __typename?: 'EventsEntity' }
    & Pick<EventsEntity, 'limit' | 'recordsLength' | 'totalRecords' | 'offset' | 'pages' | 'currentPage'>
    & { records: Array<(
      { __typename?: 'EventEntity' }
      & Pick<EventEntity, '_id' | 'name' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
      & { tickets?: Maybe<Array<(
        { __typename?: 'Ticket' }
        & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
      )>>, poster?: Maybe<(
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'sm' | 'md' | 'lg'>
      )> }
    )> }
  ) }
);

export type FetchCategoryEventsQueryVariables = {
  categoryId: Scalars['ID'];
};


export type FetchCategoryEventsQuery = (
  { __typename?: 'Query' }
  & { fetchCategoryEvents: (
    { __typename?: 'EventsEntity' }
    & Pick<EventsEntity, 'limit' | 'recordsLength' | 'totalRecords' | 'offset' | 'pages' | 'currentPage'>
    & { records: Array<(
      { __typename?: 'EventEntity' }
      & Pick<EventEntity, '_id' | 'name' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
      & { tickets?: Maybe<Array<(
        { __typename?: 'Ticket' }
        & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
      )>>, poster?: Maybe<(
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
      )> }
    )> }
  ) }
);

export type FetchRecentEventsQueryVariables = {};


export type FetchRecentEventsQuery = (
  { __typename?: 'Query' }
  & { fetchRecentEvents: (
    { __typename?: 'EventsEntity' }
    & Pick<EventsEntity, 'limit' | 'recordsLength' | 'totalRecords' | 'offset' | 'pages' | 'currentPage'>
    & { records: Array<(
      { __typename?: 'EventEntity' }
      & Pick<EventEntity, '_id' | 'name' | 'locationAccuracy' | 'startDate' | 'catchyPhrase' | 'state' | 'status'>
      & { tickets?: Maybe<Array<(
        { __typename?: 'Ticket' }
        & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
      )>>, poster?: Maybe<(
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
      )> }
    )> }
  ) }
);

export type ChangeEventStatusMutationVariables = {
  eventId: Scalars['ID'];
  status: EventStatus;
};


export type ChangeEventStatusMutation = (
  { __typename?: 'Mutation' }
  & { changeEventStatus: (
    { __typename?: 'EventEntity' }
    & Pick<EventEntity, '_id' | 'name' | 'status' | 'state' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
    & { tickets?: Maybe<Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, 'name' | 'categoryCriteria' | 'quantity' | 'price'>
    )>>, poster?: Maybe<(
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    )>, createdBy: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, 'firstName' | 'lastName'>
    ) }
  ) }
);

export type ChangeEventStateMutationVariables = {
  eventId: Scalars['ID'];
  state: EventState;
};


export type ChangeEventStateMutation = (
  { __typename?: 'Mutation' }
  & { changeEventState: (
    { __typename?: 'EventEntity' }
    & Pick<EventEntity, '_id' | 'name' | 'status' | 'state' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
    & { tickets?: Maybe<Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, 'name' | 'categoryCriteria' | 'quantity' | 'price'>
    )>>, poster?: Maybe<(
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    )>, createdBy: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, 'firstName' | 'lastName'>
    ) }
  ) }
);

export type UpdateEventMutationVariables = {
  eventId: Scalars['ID'];
  eventInput: EventDto;
};


export type UpdateEventMutation = (
  { __typename?: 'Mutation' }
  & { updateEvent: (
    { __typename?: 'EventEntity' }
    & Pick<EventEntity, '_id' | 'name' | 'description' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
    & { tickets?: Maybe<Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
    )>>, poster?: Maybe<(
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'md' | 'lg'>
    )> }
  ) }
);

export type FetchEventQueryVariables = {
  eventId: Scalars['ID'];
};


export type FetchEventQuery = (
  { __typename?: 'Query' }
  & { fetchEvent: (
    { __typename?: 'EventEntity' }
    & Pick<EventEntity, '_id' | 'name' | 'status' | 'state' | 'address' | 'locationAccuracy' | 'startDate' | 'endDate' | 'catchyPhrase' | 'description'>
    & { tickets?: Maybe<Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
    )>>, poster?: Maybe<(
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    )>, createdBy: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, 'firstName' | 'lastName'>
    ) }
  ) }
);

export type FetchUpActivePubsQueryVariables = {};


export type FetchUpActivePubsQuery = (
  { __typename?: 'Query' }
  & { fetchUpActivePubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export type FetchNoUpActivePubsQueryVariables = {};


export type FetchNoUpActivePubsQuery = (
  { __typename?: 'Query' }
  & { fetchNoUpActivePubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export type FetchNextNoUpPubsQueryVariables = {};


export type FetchNextNoUpPubsQuery = (
  { __typename?: 'Query' }
  & { fetchNextNoUpPubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export type FetchActivePubsQueryVariables = {};


export type FetchActivePubsQuery = (
  { __typename?: 'Query' }
  & { fetchActivePubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export type FetchNextPubsQueryVariables = {};


export type FetchNextPubsQuery = (
  { __typename?: 'Query' }
  & { fetchNextPubs: Array<(
    { __typename?: 'PubEntity' }
    & Pick<PubEntity, '_id' | 'attachment' | 'title' | 'object' | 'order' | 'dependence' | 'startDate' | 'endDate' | 'isUp' | 'createdAt'>
  )> }
);

export type FetchOrganizersQueryVariables = {};


export type FetchOrganizersQuery = (
  { __typename?: 'Query' }
  & { fetchOrganizers: (
    { __typename?: 'UsersEntity' }
    & { records: Array<(
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName' | 'email' | 'role' | 'internalRole' | 'password' | 'state'>
    )> }
  ) }
);

export type FetchEventReservationsQueryVariables = {
  eventId: Scalars['ID'];
};


export type FetchEventReservationsQuery = (
  { __typename?: 'Query' }
  & { fetchEventReservations: Array<(
    { __typename?: 'ReservationEntity' }
    & Pick<ReservationEntity, 'createdAt'>
    & { event: (
      { __typename?: 'EventEntity' }
      & Pick<EventEntity, 'name'>
    ), ticket: (
      { __typename?: 'Ticket' }
      & Pick<Ticket, 'name' | 'price'>
    ), client: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'email' | 'phoneNumber'>
    ), card: (
      { __typename?: 'CardEntity' }
      & Pick<CardEntity, 'number'>
    ) }
  )> }
);

export type CreatePromotionalCodeMutationVariables = {
  promotionalCodeDto: PromotionalCodeDto;
};


export type CreatePromotionalCodeMutation = (
  { __typename?: 'Mutation' }
  & { createPromotionalCode: (
    { __typename?: 'PromotionalCodeEntity' }
    & Pick<PromotionalCodeEntity, '_id' | 'eventId' | 'impact' | 'privateName' | 'numberOfGeneratedCodes' | 'publicName' | 'usableNumberOfTimes'>
    & { tickets: Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, '_id' | 'name'>
    )>, reductionEffect: (
      { __typename?: 'ReductionEffectEntity' }
      & Pick<ReductionEffectEntity, 'inPercentage' | 'reduction'>
    ) }
  ) }
);

export type FetchPromotionalCodesQueryVariables = {};


export type FetchPromotionalCodesQuery = (
  { __typename?: 'Query' }
  & { fetchPromotionalCodes: (
    { __typename?: 'PromotionalCodesEntity' }
    & { records: Array<(
      { __typename?: 'PromotionalCodeEntity' }
      & Pick<PromotionalCodeEntity, '_id' | 'impact' | 'privateName' | 'numberOfGeneratedCodes' | 'publicName' | 'usableNumberOfTimes'>
      & { event: (
        { __typename?: 'EventEntity' }
        & Pick<EventEntity, '_id' | 'name'>
      ), tickets: Array<(
        { __typename?: 'Ticket' }
        & Pick<Ticket, '_id' | 'name'>
      )>, reductionEffect: (
        { __typename?: 'ReductionEffectEntity' }
        & Pick<ReductionEffectEntity, 'inPercentage' | 'reduction'>
      ) }
    )> }
  ) }
);

export type CreateTicketMutationVariables = {
  eventId: Scalars['ID'];
  ticketDto: TicketDto;
};


export type CreateTicketMutation = (
  { __typename?: 'Mutation' }
  & { createTicket: (
    { __typename?: 'EventEntity' }
    & Pick<EventEntity, '_id' | 'name' | 'status' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
    & { tickets?: Maybe<Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
    )>>, poster?: Maybe<(
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    )>, createdBy: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, 'firstName' | 'lastName'>
    ) }
  ) }
);

export type RemoveTicketMutationVariables = {
  eventId: Scalars['ID'];
  ticketId: Scalars['ID'];
};


export type RemoveTicketMutation = (
  { __typename?: 'Mutation' }
  & { removeTicket: (
    { __typename?: 'EventEntity' }
    & Pick<EventEntity, '_id' | 'name' | 'status' | 'locationAccuracy' | 'startDate' | 'catchyPhrase'>
    & { tickets?: Maybe<Array<(
      { __typename?: 'Ticket' }
      & Pick<Ticket, '_id' | 'name' | 'categoryCriteria' | 'quantity' | 'price'>
    )>>, poster?: Maybe<(
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    )>, createdBy: (
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, 'firstName' | 'lastName'>
    ) }
  ) }
);

export type FetchCurrentUserQueryVariables = {};


export type FetchCurrentUserQuery = (
  { __typename?: 'Query' }
  & { fetchCurrentUser: (
    { __typename?: 'UserEntity' }
    & Pick<UserEntity, '_id' | 'firstName' | 'lastName' | 'email' | 'role'>
    & { avatar?: Maybe<(
      { __typename?: 'ImageSizes' }
      & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
    )> }
  ) }
);

export type FetchAdminsQueryVariables = {};


export type FetchAdminsQuery = (
  { __typename?: 'Query' }
  & { fetchAdmins: (
    { __typename?: 'UsersEntity' }
    & { records: Array<(
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName' | 'email' | 'role' | 'internalRole' | 'password'>
      & { avatar?: Maybe<(
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
      )> }
    )> }
  ) }
);

export type FetchClientsQueryVariables = {};


export type FetchClientsQuery = (
  { __typename?: 'Query' }
  & { fetchClients: (
    { __typename?: 'UsersEntity' }
    & { records: Array<(
      { __typename?: 'UserEntity' }
      & Pick<UserEntity, '_id' | 'firstName' | 'lastName' | 'birthDate' | 'phoneNumber' | 'city' | 'district' | 'email' | 'role' | 'internalRole'>
      & { avatar?: Maybe<(
        { __typename?: 'ImageSizes' }
        & Pick<ImageSizes, 'sm' | 'lg' | 'md'>
      )> }
    )> }
  ) }
);

export type CloseAccountMutationVariables = {
  userId?: Maybe<Scalars['ID']>;
};


export type CloseAccountMutation = (
  { __typename?: 'Mutation' }
  & Pick<Mutation, 'closeAccount'>
);

export const FetchCategoriesDocument = gql`
    query FetchCategories {
  fetchCategories {
    records {
      _id
      name
      validated
      refused
      pending
      activated
      desactivated
      archived
      paidEntrance
      freeEntrance
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchCategoriesGQL extends Apollo.Query<FetchCategoriesQuery, FetchCategoriesQueryVariables> {
    document = FetchCategoriesDocument;
    
  }
export const CreateCategoryDocument = gql`
    mutation CreateCategory($categoryInput: CategoryInput!) {
  createCategory(categoryInput: $categoryInput) {
    _id
    name
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateCategoryGQL extends Apollo.Mutation<CreateCategoryMutation, CreateCategoryMutationVariables> {
    document = CreateCategoryDocument;
    
  }
export const FetchAccessCodesDocument = gql`
    query FetchAccessCodes {
  fetchAccessCodes {
    records {
      _id
      username
      password
      role
      organizer {
        firstName
        lastName
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchAccessCodesGQL extends Apollo.Query<FetchAccessCodesQuery, FetchAccessCodesQueryVariables> {
    document = FetchAccessCodesDocument;
    
  }
export const CreateAccessCodeDocument = gql`
    mutation CreateAccessCode($accessCodeDto: AccessCodeDto!) {
  createAccessCode(accessCodeDto: $accessCodeDto) {
    _id
    username
    password
    role
    organizer {
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateAccessCodeGQL extends Apollo.Mutation<CreateAccessCodeMutation, CreateAccessCodeMutationVariables> {
    document = CreateAccessCodeDocument;
    
  }
export const FetchEventsDocument = gql`
    query FetchEvents {
  fetchEvents {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        md
        lg
      }
      locationAccuracy
      startDate
      catchyPhrase
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchEventsGQL extends Apollo.Query<FetchEventsQuery, FetchEventsQueryVariables> {
    document = FetchEventsDocument;
    
  }
export const FetchCategoryEventsDocument = gql`
    query FetchCategoryEvents($categoryId: ID!) {
  fetchCategoryEvents(categoryId: $categoryId) {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      locationAccuracy
      startDate
      catchyPhrase
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchCategoryEventsGQL extends Apollo.Query<FetchCategoryEventsQuery, FetchCategoryEventsQueryVariables> {
    document = FetchCategoryEventsDocument;
    
  }
export const FetchRecentEventsDocument = gql`
    query FetchRecentEvents {
  fetchRecentEvents {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      locationAccuracy
      startDate
      catchyPhrase
      state
      status
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchRecentEventsGQL extends Apollo.Query<FetchRecentEventsQuery, FetchRecentEventsQueryVariables> {
    document = FetchRecentEventsDocument;
    
  }
export const ChangeEventStatusDocument = gql`
    mutation ChangeEventStatus($eventId: ID!, $status: EventStatus!) {
  changeEventStatus(eventId: $eventId, status: $status) {
    _id
    name
    tickets {
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ChangeEventStatusGQL extends Apollo.Mutation<ChangeEventStatusMutation, ChangeEventStatusMutationVariables> {
    document = ChangeEventStatusDocument;
    
  }
export const ChangeEventStateDocument = gql`
    mutation ChangeEventState($eventId: ID!, $state: EventState!) {
  changeEventState(eventId: $eventId, state: $state) {
    _id
    name
    tickets {
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ChangeEventStateGQL extends Apollo.Mutation<ChangeEventStateMutation, ChangeEventStateMutationVariables> {
    document = ChangeEventStateDocument;
    
  }
export const UpdateEventDocument = gql`
    mutation UpdateEvent($eventId: ID!, $eventInput: EventDto!) {
  updateEvent(eventId: $eventId, eventInput: $eventInput) {
    _id
    name
    description
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      md
      lg
    }
    locationAccuracy
    startDate
    catchyPhrase
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UpdateEventGQL extends Apollo.Mutation<UpdateEventMutation, UpdateEventMutationVariables> {
    document = UpdateEventDocument;
    
  }
export const FetchEventDocument = gql`
    query FetchEvent($eventId: ID!) {
  fetchEvent(eventId: $eventId) {
    _id
    name
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    address
    locationAccuracy
    startDate
    endDate
    catchyPhrase
    description
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchEventGQL extends Apollo.Query<FetchEventQuery, FetchEventQueryVariables> {
    document = FetchEventDocument;
    
  }
export const FetchUpActivePubsDocument = gql`
    query FetchUpActivePubs {
  fetchUpActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchUpActivePubsGQL extends Apollo.Query<FetchUpActivePubsQuery, FetchUpActivePubsQueryVariables> {
    document = FetchUpActivePubsDocument;
    
  }
export const FetchNoUpActivePubsDocument = gql`
    query FetchNoUpActivePubs {
  fetchNoUpActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchNoUpActivePubsGQL extends Apollo.Query<FetchNoUpActivePubsQuery, FetchNoUpActivePubsQueryVariables> {
    document = FetchNoUpActivePubsDocument;
    
  }
export const FetchNextNoUpPubsDocument = gql`
    query FetchNextNoUpPubs {
  fetchNextNoUpPubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchNextNoUpPubsGQL extends Apollo.Query<FetchNextNoUpPubsQuery, FetchNextNoUpPubsQueryVariables> {
    document = FetchNextNoUpPubsDocument;
    
  }
export const FetchActivePubsDocument = gql`
    query FetchActivePubs {
  fetchActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchActivePubsGQL extends Apollo.Query<FetchActivePubsQuery, FetchActivePubsQueryVariables> {
    document = FetchActivePubsDocument;
    
  }
export const FetchNextPubsDocument = gql`
    query FetchNextPubs {
  fetchNextPubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchNextPubsGQL extends Apollo.Query<FetchNextPubsQuery, FetchNextPubsQueryVariables> {
    document = FetchNextPubsDocument;
    
  }
export const FetchOrganizersDocument = gql`
    query FetchOrganizers {
  fetchOrganizers {
    records {
      _id
      firstName
      lastName
      email
      role
      internalRole
      password
      state
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchOrganizersGQL extends Apollo.Query<FetchOrganizersQuery, FetchOrganizersQueryVariables> {
    document = FetchOrganizersDocument;
    
  }
export const FetchEventReservationsDocument = gql`
    query FetchEventReservations($eventId: ID!) {
  fetchEventReservations(eventId: $eventId) {
    event {
      name
    }
    ticket {
      name
      price
    }
    client {
      _id
      email
      phoneNumber
    }
    card {
      number
    }
    createdAt
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchEventReservationsGQL extends Apollo.Query<FetchEventReservationsQuery, FetchEventReservationsQueryVariables> {
    document = FetchEventReservationsDocument;
    
  }
export const CreatePromotionalCodeDocument = gql`
    mutation CreatePromotionalCode($promotionalCodeDto: PromotionalCodeDto!) {
  createPromotionalCode(promotionalCodeDto: $promotionalCodeDto) {
    _id
    eventId
    impact
    privateName
    numberOfGeneratedCodes
    tickets {
      _id
      name
    }
    reductionEffect {
      inPercentage
      reduction
    }
    publicName
    usableNumberOfTimes
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreatePromotionalCodeGQL extends Apollo.Mutation<CreatePromotionalCodeMutation, CreatePromotionalCodeMutationVariables> {
    document = CreatePromotionalCodeDocument;
    
  }
export const FetchPromotionalCodesDocument = gql`
    query FetchPromotionalCodes {
  fetchPromotionalCodes {
    records {
      _id
      event {
        _id
        name
      }
      impact
      privateName
      numberOfGeneratedCodes
      tickets {
        _id
        name
      }
      reductionEffect {
        inPercentage
        reduction
      }
      publicName
      usableNumberOfTimes
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchPromotionalCodesGQL extends Apollo.Query<FetchPromotionalCodesQuery, FetchPromotionalCodesQueryVariables> {
    document = FetchPromotionalCodesDocument;
    
  }
export const CreateTicketDocument = gql`
    mutation CreateTicket($eventId: ID!, $ticketDto: TicketDto!) {
  createTicket(eventId: $eventId, ticketDto: $ticketDto) {
    _id
    name
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CreateTicketGQL extends Apollo.Mutation<CreateTicketMutation, CreateTicketMutationVariables> {
    document = CreateTicketDocument;
    
  }
export const RemoveTicketDocument = gql`
    mutation RemoveTicket($eventId: ID!, $ticketId: ID!) {
  removeTicket(eventId: $eventId, ticketId: $ticketId) {
    _id
    name
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class RemoveTicketGQL extends Apollo.Mutation<RemoveTicketMutation, RemoveTicketMutationVariables> {
    document = RemoveTicketDocument;
    
  }
export const FetchCurrentUserDocument = gql`
    query FetchCurrentUser {
  fetchCurrentUser {
    _id
    firstName
    lastName
    email
    role
    avatar {
      sm
      lg
      md
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchCurrentUserGQL extends Apollo.Query<FetchCurrentUserQuery, FetchCurrentUserQueryVariables> {
    document = FetchCurrentUserDocument;
    
  }
export const FetchAdminsDocument = gql`
    query FetchAdmins {
  fetchAdmins {
    records {
      _id
      firstName
      lastName
      email
      role
      internalRole
      password
      avatar {
        sm
        lg
        md
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchAdminsGQL extends Apollo.Query<FetchAdminsQuery, FetchAdminsQueryVariables> {
    document = FetchAdminsDocument;
    
  }
export const FetchClientsDocument = gql`
    query FetchClients {
  fetchClients {
    records {
      _id
      firstName
      lastName
      birthDate
      phoneNumber
      city
      district
      email
      role
      internalRole
      avatar {
        sm
        lg
        md
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class FetchClientsGQL extends Apollo.Query<FetchClientsQuery, FetchClientsQueryVariables> {
    document = FetchClientsDocument;
    
  }
export const CloseAccountDocument = gql`
    mutation CloseAccount($userId: ID) {
  closeAccount(userId: $userId)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CloseAccountGQL extends Apollo.Mutation<CloseAccountMutation, CloseAccountMutationVariables> {
    document = CloseAccountDocument;
    
  }

export const FetchCategories = gql`
    query FetchCategories {
  fetchCategories {
    records {
      _id
      name
      validated
      refused
      pending
      activated
      desactivated
      archived
      paidEntrance
      freeEntrance
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;
export const CreateCategory = gql`
    mutation CreateCategory($categoryInput: CategoryInput!) {
  createCategory(categoryInput: $categoryInput) {
    _id
    name
  }
}
    `;
export const FetchAccessCodes = gql`
    query FetchAccessCodes {
  fetchAccessCodes {
    records {
      _id
      username
      password
      role
      organizer {
        firstName
        lastName
      }
    }
  }
}
    `;
export const CreateAccessCode = gql`
    mutation CreateAccessCode($accessCodeDto: AccessCodeDto!) {
  createAccessCode(accessCodeDto: $accessCodeDto) {
    _id
    username
    password
    role
    organizer {
      firstName
      lastName
    }
  }
}
    `;
export const FetchEvents = gql`
    query FetchEvents {
  fetchEvents {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        md
        lg
      }
      locationAccuracy
      startDate
      catchyPhrase
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;
export const FetchCategoryEvents = gql`
    query FetchCategoryEvents($categoryId: ID!) {
  fetchCategoryEvents(categoryId: $categoryId) {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      locationAccuracy
      startDate
      catchyPhrase
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;
export const FetchRecentEvents = gql`
    query FetchRecentEvents {
  fetchRecentEvents {
    records {
      _id
      name
      tickets {
        _id
        name
        categoryCriteria
        quantity
        price
      }
      poster {
        sm
        lg
        md
      }
      locationAccuracy
      startDate
      catchyPhrase
      state
      status
    }
    limit
    recordsLength
    totalRecords
    offset
    pages
    currentPage
  }
}
    `;
export const ChangeEventStatus = gql`
    mutation ChangeEventStatus($eventId: ID!, $status: EventStatus!) {
  changeEventStatus(eventId: $eventId, status: $status) {
    _id
    name
    tickets {
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;
export const ChangeEventState = gql`
    mutation ChangeEventState($eventId: ID!, $state: EventState!) {
  changeEventState(eventId: $eventId, state: $state) {
    _id
    name
    tickets {
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;
export const UpdateEvent = gql`
    mutation UpdateEvent($eventId: ID!, $eventInput: EventDto!) {
  updateEvent(eventId: $eventId, eventInput: $eventInput) {
    _id
    name
    description
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      md
      lg
    }
    locationAccuracy
    startDate
    catchyPhrase
  }
}
    `;
export const FetchEvent = gql`
    query FetchEvent($eventId: ID!) {
  fetchEvent(eventId: $eventId) {
    _id
    name
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    state
    address
    locationAccuracy
    startDate
    endDate
    catchyPhrase
    description
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;
export const FetchUpActivePubs = gql`
    query FetchUpActivePubs {
  fetchUpActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;
export const FetchNoUpActivePubs = gql`
    query FetchNoUpActivePubs {
  fetchNoUpActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;
export const FetchNextNoUpPubs = gql`
    query FetchNextNoUpPubs {
  fetchNextNoUpPubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;
export const FetchActivePubs = gql`
    query FetchActivePubs {
  fetchActivePubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;
export const FetchNextPubs = gql`
    query FetchNextPubs {
  fetchNextPubs {
    _id
    attachment
    title
    object
    order
    dependence
    startDate
    endDate
    isUp
    createdAt
  }
}
    `;
export const FetchOrganizers = gql`
    query FetchOrganizers {
  fetchOrganizers {
    records {
      _id
      firstName
      lastName
      email
      role
      internalRole
      password
      state
    }
  }
}
    `;
export const FetchEventReservations = gql`
    query FetchEventReservations($eventId: ID!) {
  fetchEventReservations(eventId: $eventId) {
    event {
      name
    }
    ticket {
      name
      price
    }
    client {
      _id
      email
      phoneNumber
    }
    card {
      number
    }
    createdAt
  }
}
    `;
export const CreatePromotionalCode = gql`
    mutation CreatePromotionalCode($promotionalCodeDto: PromotionalCodeDto!) {
  createPromotionalCode(promotionalCodeDto: $promotionalCodeDto) {
    _id
    eventId
    impact
    privateName
    numberOfGeneratedCodes
    tickets {
      _id
      name
    }
    reductionEffect {
      inPercentage
      reduction
    }
    publicName
    usableNumberOfTimes
  }
}
    `;
export const FetchPromotionalCodes = gql`
    query FetchPromotionalCodes {
  fetchPromotionalCodes {
    records {
      _id
      event {
        _id
        name
      }
      impact
      privateName
      numberOfGeneratedCodes
      tickets {
        _id
        name
      }
      reductionEffect {
        inPercentage
        reduction
      }
      publicName
      usableNumberOfTimes
    }
  }
}
    `;
export const CreateTicket = gql`
    mutation CreateTicket($eventId: ID!, $ticketDto: TicketDto!) {
  createTicket(eventId: $eventId, ticketDto: $ticketDto) {
    _id
    name
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;
export const RemoveTicket = gql`
    mutation RemoveTicket($eventId: ID!, $ticketId: ID!) {
  removeTicket(eventId: $eventId, ticketId: $ticketId) {
    _id
    name
    tickets {
      _id
      name
      categoryCriteria
      quantity
      price
    }
    poster {
      sm
      lg
      md
    }
    status
    locationAccuracy
    startDate
    catchyPhrase
    createdBy {
      firstName
      lastName
    }
  }
}
    `;
export const FetchCurrentUser = gql`
    query FetchCurrentUser {
  fetchCurrentUser {
    _id
    firstName
    lastName
    email
    role
    avatar {
      sm
      lg
      md
    }
  }
}
    `;
export const FetchAdmins = gql`
    query FetchAdmins {
  fetchAdmins {
    records {
      _id
      firstName
      lastName
      email
      role
      internalRole
      password
      avatar {
        sm
        lg
        md
      }
    }
  }
}
    `;
export const FetchClients = gql`
    query FetchClients {
  fetchClients {
    records {
      _id
      firstName
      lastName
      birthDate
      phoneNumber
      city
      district
      email
      role
      internalRole
      avatar {
        sm
        lg
        md
      }
    }
  }
}
    `;
export const CloseAccount = gql`
    mutation CloseAccount($userId: ID) {
  closeAccount(userId: $userId)
}
    `;

      export interface IntrospectionResultData {
        __schema: {
          types: {
            kind: string;
            name: string;
            possibleTypes: {
              name: string;
            }[];
          }[];
        };
      }
      const result: IntrospectionResultData = {
  "__schema": {
    "types": []
  }
};
      export default result;
    